### -*-Makefile-*- pour compiler les diapositives du cours 
###                ACT-2008 Mathématiques actuarielles IARD II
##
## Copyright (C) 2018-2023 Vincent Goulet
##
## 'make pdf' crée les fichiers .tex à partir des fichiers .Rnw avec
## Sweave et compile le document maitre avec XeLaTeX.
##
## 'make tex' crée les fichiers .tex à partir des fichiers .Rnw avec
## Sweave.
##
## 'make scripts' crée les fichiers .R à partir des fichiers .Rnw avec
## Sweave (et Stangle).
##
## 'make update-copyright' met à jour l'année de copyright dans toutes
## les sources du document
##
## 'make zip' crée l'archive de la distribution.
##
## 'make release' crée une nouvelle version dans GitLab, téléverse le
## fichier .zip et modifie les liens de la page web.
##
## 'make check-url' vérifie la validité de toutes les url présentes
## dans les sources du document.
##
## 'make all' est équivalent à 'make pdf' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet
## «ACT-2008 Mathématiques actuarielles IARD II»
## https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git


## Bash requis pour 'process substitution'
SHELL := /bin/bash

## Fichier principal
MASTER = mathematiques-actuarielles-iard-ii.pdf
ARCHIVE = ${MASTER:.pdf=.zip}
README = README.md
NEWS = NEWS
LICENSE = LICENSE

## Le document maitre dépend de tous les fichiers .Rnw et de tous les
## fichiers .tex autres que lui-même qui n'ont pas de version .Rnw.
RNWFILES = $(wildcard *.Rnw)
TEXFILES = $(addsuffix .tex,$(filter-out $(basename ${RNWFILES} ${MASTER}),\
                                         $(basename $(wildcard *.tex))))	

## Les diapositives sont aussi livrées avec des fichiers de script et
## des fichiers de données.
SCRIPTS = chain-ladder.R bornhuetter-ferguson.R mack.R regression.R
DATA = mack-cas.csv \
	exercice-chain-ladder.csv exercice-bornhuetter-ferguson.csv \
	exercice-mack-1.csv exercice-mack-2.csv

## Informations de publication extraites du fichier maitre
REPOSURL = $(shell grep "newcommand{\\\\reposurl" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
YEAR = $(shell grep "newcommand{\\\\year" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
MONTH = $(shell grep "newcommand{\\\\month" ${MASTER:.pdf=.tex} \
	| cut -d } -f 2 | tr -d {)
VERSION = ${YEAR}.${MONTH}

## Outils de travail
SWEAVE = R CMD SWEAVE --encoding="utf-8"
TEXI2DVI = LATEX=xelatex texi2dvi -b
CP = cp -p
RM = rm -rf

## Dossier temporaire pour construire l'archive
BUILDDIR = tmpdir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variable automatique
TAGNAME = v${VERSION}


all: pdf

%.tex: %.Rnw
	${SWEAVE} '$<'

${MASTER}: ${MASTER:.pdf=.tex} ${RNWFILES:.Rnw=.tex} ${TEXFILES} \
	   ${wildcard images/*}
	${TEXI2DVI} ${MASTER:.pdf=.tex}

.PHONY: pdf
pdf: ${MASTER}

.PHONY: tex
tex: ${RNWFILES:.Rnw=.tex}

.PHONY: release
release: update-copyright zip check-status create-release upload create-link

.PHONY: update-copyright
update-copyright: ${MASTER:.pdf=.tex} ${RNWFILES} ${TEXFILES} ${SCRIPTS}
	for f in $?; \
	    do sed -E '/^(#|%)* +Copyright \(C\)/s/-20[0-9]{2}/-$(shell date "+%Y")/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

zip: ${MASTER} ${README} ${NEWS} ${SCRIPTS} ${DATA} ${LICENSE}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	mkdir -p ${BUILDDIR}
	touch ${BUILDDIR}/${README} && \
	  awk 'state==0 && /^# / { state=1 }; \
	       /^## Auteur/ { printf("## Édition\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${README}
	cp ${MASTER} ${SCRIPTS} ${DATA} ${NEWS} ${LICENSE} \
	   ${BUILDDIR}
	cd ${BUILDDIR} && zip --filesync -r ../${ARCHIVE} *
	${RM} ${BUILDDIR}

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "vérification de l'état du dépôt local... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "! pas sur la branche main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "! changements non archivés dans le dépôt"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "changements non publiés dans le dépôt; publication dans origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "vérification que la version existe déjà... "; \
	    http_code=$$(curl -I "${APIURL}/releases/${TAGNAME}" 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "oui"; \
	        printf "%s\n" "-> utilisation de la version actuelle"; \
	    else \
	        printf "%s\n" "non"; \
	        printf "%s" "création d'une version dans GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print "Édition", $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s\n" "téléversement de l'archive vers le registre..."
	curl --upload-file "${ARCHIVE}" \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}/packages/generic/${REPOSNAME}/${VERSION}/${ARCHIVE}"
	@printf "\n%s\n" "ok"

.PHONY: create-link
create-link:
	@printf "%s\n" "ajout du lien dans la description de la version..."
	$(eval pkg_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval file_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                            --silent \
	                            "${APIURL}/packages/${pkg_id}/package_files" \
	                       | grep -o -E '\{[^{]*"file_name":"${ARCHIVE}"[^}]*}' \
	                       | grep -o '"id":[0-9]*' | cut -d: -f 2))
	curl --request POST \
	      --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	      --data name="${ARCHIVE}" \
	      --data url="${REPOSURL}/-/package_files/${file_id}/download" \
	      --data link_type="package" \
	      --output /dev/null --silent \
	      "${APIURL}/releases/${TAGNAME}/assets/links"
	@printf "%s\n" "ok"

.PHONY: publish
publish:
	@printf "%s" "mise à jour de la page web..."
	git checkout pages && \
	  ${MAKE} && \
	  git checkout master
	@printf "%s\n" "ok"

.PHONY: check-url
check-url: ${MASTER:.pdf=.tex} ${RNWFILES} ${TEXFILES}
	@printf "%s\n" "vérification des adresses URL dans les fichiers source"
	$(eval url=$(shell grep -E -o -h 'https?:\/\/[^./]+(?:\.[^./]+)+(?:\/[^ ]*)?' $? \
		   | cut -d \} -f 1 \
		   | cut -d ] -f 1 \
		   | cut -d '"' -f 1 \
		   | sort | uniq))
	@for u in ${url}; do \
	    printf "%s... " "$$u"; \
	    if curl --output /dev/null --silent --head --fail --max-time 5 "$$u"; then \
	        printf "%s\n" "ok"; \
	    else \
		printf "%s\n" "invalide ou ne répond pas"; \
	    fi; \
	done

.PHONY: clean
clean:
	${RM} *.log *.nav *.out *.snm *.vrb *.toc *~ Rplots* .RData

.PHONY: clean-all
clean-all: clean
	${RM} ${MASTER} \
	      ${RNWFILES:.Rnw=.tex} \
	      *.aux
