<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Mathématiques actuarielles IARD II

Ce projet contient les diapositives du cours [ACT-2008](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/act-2008-mathematiques-actuarielles-iard-ii.html)
de l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université
Laval](https://ulaval.ca).

Le second cours de mathématiques actuarielles IARD du baccalauréat en actuariat traite de deux sujets au cœur des opérations en assurance de dommages: la tarification et le calcul des provisions pour sinistres. Les objectifs généraux du cours sont:

1. Comprendre les grandes étapes de la tarification et du provisionnement en assurance IARD.
2. Utiliser les modèles classiques de théorie de la crédibilité dans un contexte de données hétérogènes.
3. Utiliser les techniques déterministes et stochastiques pour établir les provisions pour sinistres en assurance IARD.
4. Appliquer la théorie de la crédibilité dans un contexte de données hétorogènes autre que celui de l'assurance.

Les ouvrages de référence du cours sont [*Théorie de la crédibilité avec R*](https://vigou3.gitlab.io/theorie-credibilite-avec-r) et *Provisionnement en assurance IARD* (à venir).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Mathématiques actuarielles IARD II» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`mathematiques-actuarielles-iard-ii`](https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`mathematiques-actuarielles-iard-ii-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/mathematiques-actuarielles-iard-ii-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `mathematiques-actuarielles-iard-ii-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.

## Obtenir les diapositives

Pour obtenir les diapositives en format PDF, consulter la [page des versions](https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii/-/releases) (*releases*).
