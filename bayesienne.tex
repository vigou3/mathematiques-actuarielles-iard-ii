%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Tarification bayésienne}

\begin{frame}
  \frametitle{Mise en situation}

  \begin{itemize}
  \item Trois \alert{risques} (assurés, contrats, \dots) à priori
    identiques
  \item Expérience dans l'année: sinistre ou non ($0$ ou $1$)
  \item $S \sim \text{Bernoulli}(\theta)$
  \item But: estimer $\theta$ ou une fonction de $\theta$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mise en situation ---  Approche classique}
  \frametitle{}

  \begin{itemize}
  \item Estimateurs développés à partir d'un
    critère objectif (absence de biais, maximum de vraisemblance,
    etc.)
    \begin{equation*}
      \hat{\theta}^{\text{MLE}} = \frac{1}{n} \sum_{t = 1}^n S_t
    \end{equation*}
  \item Que faire la première année?
  \item Laissons-nous de côté de l'information utile dans les
    \alert{données collatérales}?
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mise en situation --- Approche bayésienne}

  \begin{itemize}
  \item Opinion sur la valeur de $\theta$ prise en compte
  \item Incertitude sur la valeur de $\theta$: paramètre est une
    réalisation d'une variable aléatoire $\Theta$
  \item Nous allons poser
    \begin{equation*}
      \Pr[\Theta = \theta] =
      \begin{cases}
        \frac{1}{3}, & \theta = 0,3 \\[4pt]
        \frac{1}{3}, & \theta = 0,5 \\[4pt]
        \frac{1}{3}, & \theta = 0,8
      \end{cases}
    \end{equation*}
  \item Distribution à priori de $\theta$ révisée et améliorée à
    l'aide de la \alert{règle de Bayes} lorsque l'information
    s'accumule
  \end{itemize}
\end{frame}

\begin{frame}[standout]
  Démo
\end{frame}

\begin{frame}
  \frametitle{Modèle d'hétérogénéité}

  \begin{itemize}
  \item Modèle classique de crédibilité de précision établi par
    \citet{Buhlmann:credibility:1967}
  \item Groupe (portefeuille) d'assurés \alert{hétérogène}
  \item Chaque assuré a un \alert{niveau de risque} inconnu et non
    observable représenté par $\theta_i$, une réalisation de la
    variable $\Theta_i$
  \item Hypothèses:
    \begin{enumerate}
    \item sinistres de l'assuré $i$ \alert{conditionnellement}
      indépendants et identiquement distribués
    \item variables aléatoires $\Theta_1, \dots, \Theta_I$
      identiquement distribuées
    \item assurés indépendants
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Retour à la mise en situation}
  \centering

  Quelle prime charger?
\end{frame}

\begin{frame}
  \frametitle{Deux premières solutions}

  \begin{itemize}
  \item Prime idéale: \alert{prime de risque}
    \begin{equation*}
      \mu(\theta) = \esp{S|\Theta = \theta}
    \end{equation*}

    Problème: inconnue
  \item Solution pour la première année: \alert{prime collective}
    \begin{align*}
      m &= \esp{\mu(\Theta)}
         = \sum_\theta \mu(\theta) \Pr[\Theta = \theta]
    \end{align*}
    Problème: pas équitable à long terme, antisélection
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Meilleure solution à long terme}

  \begin{itemize}
  \item Indépendance des assurés: résultats d'un assuré sans effet
    sur la prime d'un autre (pour le moment!)
  \item Nous avons des observations $x_1, x_2, x_n$ disponibles pour
    la prévision
  \item Nous cherchons la «meilleure» approximation de la prime de
    risque utilisant les données $x_1, \dots, x_n$ d'un assuré:
    \begin{equation*}
      \esp{(\mu(\Theta) - g(x_1, \dots, x_n))^2} = \min !
    \end{equation*}
  \item Fonction qui minimise l'espérance est la \alert{prime
      bayésienne}
    \begin{align*}
      B_{n + 1}
      &= \esp{\mu(\Theta)|S_1 = x_1, \dots, S_n = x_n} \\
      &= \sum_\theta \mu(\theta) \Pr[\Theta = \theta|S_1 = x_1, \dots, S_n = x_n]
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mise en situation --- Petit changement}

  \begin{itemize}
  \item Nous posons maintenant $\Theta \sim \text{Bêta}(a, b)$:
     \begin{equation*}
       u(\theta) = \frac{\Gamma(a + b)}{\Gamma(a) \Gamma(b)}
       \theta^{a - 1} (1 - \theta)^{b - 1}, \quad 0 < \theta < 1
     \end{equation*}
   \item Prime de risque
     \begin{equation*}
       \mu(\theta) = \esp{S|\Theta = \theta} = \theta
     \end{equation*}
   \item Prime collective
     \begin{equation*}
       m = \esp{\mu(\Theta)} = \esp{\Theta} = \frac{a}{a + b}
     \end{equation*}
   \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Calcul de la distribution à postériori}

  \begin{itemize}
  \item Nous avons
    \begin{align*}
      f(x|\theta)
      &= \theta^x (1 - \theta)^{1 - x}, \quad x = 0, 1 \\
      u(\theta)
      &= \frac{\Gamma(a + b)}{\Gamma(a) \Gamma(b)}\,
        \theta^{a - 1} (1 - \theta)^{b - 1},
        \quad 0 < \theta < 1 \\
    \end{align*}
  \item Par indépendance conditionnelle des sinistres
    \begin{align*}
      u(\theta|x_1, \dots, x_n)
      &= \frac{u(\theta) \prod_{t = 1}^n f(x_t|\theta)}{%
        \int_{-\infty}^\infty u(\theta) \prod_{t = 1}^n f(x_t|\theta) d\theta} \\
      &\propto \theta^{a - 1} (1 - \theta)^{b - 1}
        \prod_{t = 1}^n \theta^{x_t} (1 - \theta)^{1 - x_t} \\
      &= \theta^{a + \sum_{t = 1}^n x_t - 1} (1 - \theta)^{b
        + n - \sum_{t = 1}^n x_t - 1}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Prime bayésienne après $n$ années}
  \begin{itemize}
  \item La distribution à posteriori est
    \begin{equation*}
      \textstyle
      \Theta|S_1 = x_1, \dots, S_n = x_n
      \sim \text{Bêta}(\tilde{a} = a + \sum_{t = 1}^n x_t,
      \tilde{b} = b + n - \sum_{t = 1}^n x_t)
    \end{equation*}
  \item La prime bayésienne pour l'année $n + 1$ est donc
    \begin{align*}
      B_{n + 1}
      &= \esp{\mu(\Theta)|S_1, \dots, S_n} \\
      &= \esp{\Theta|S_1, \dots, S_n} \\
      &= \frac{\tilde{a}}{\tilde{a} + \tilde{b}} \\
      &= \frac{a + \sum_{t = 1}^n S_t}{a + b + n}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Crédibilité bayésienne linéaire (ou exacte)}

  Notre prime bayésienne peut se réécrire sous la forme
  \begin{align*}
    B_{n + 1}
    &= \frac{a + \sum_{t = 1}^n S_t}{a + b + n} \\
    &= \frac{n}{n + a + b}\, \bar{S} +
    \frac{a + b}{n + a + b} \frac{a}{a + b} \\
    &= z \bar{S} + (1 - z) m \\
    \intertext{avec}
    z &= \frac{n}{n + K}, \quad K = a + b
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Prime de crédibilité}

  \begin{itemize}
  \item Une prime de la forme
    \begin{displaymath}
      \pi_{n + 1} = z \bar{S} + (1 - z) m
    \end{displaymath}
    est appelée \alert{prime de crédibilité}
  \item $0 \leq z \leq 1$ est le \alert{facteur de crédibilité}
  \item Whitney (1918) et Bailey (1950) les premiers à démontrer que
    la prime bayésienne est parfois une prime de crédibilité
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interprétation}

  Dans tous les cas où la prime bayésienne est linéaire, on a
  \begin{equation*}
    z = \frac{n}{n + K}
  \end{equation*}
  \begin{itemize}
  \item $z \rightarrow 1$ quand $K \rightarrow 0$
    \begin{itemize}
    \item grande incertitude quant à la valeur de $\theta$
    \item se fier à l'expérience individuelle
    \end{itemize}
  \item $z \rightarrow 0$ quand $K \rightarrow \infty$
    \begin{itemize}
    \item niveau de risque presque connu avec certitude
    \item prime «collective» adéquate
    \end{itemize}
  \item $z \rightarrow 1$ quand $n \rightarrow \infty$
    \begin{itemize}
    \item fiabilité de l'expérience individuelle augmente avec le
      nombre d'années
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Cinq cas principaux de prime bayésienne linéaire}

  \begin{enumerate}
  \item $S|\Theta = \theta \sim \text{Poisson}(\theta)$ et $\Theta \sim
    \text{Gamma}(\alpha, \lambda)$
  \item $S|\Theta = \theta \sim \text{Exponentielle}(\theta)$ et $\Theta \sim
    \text{Gamma}(\alpha, \lambda)$
  \item $S|\Theta = \theta \sim \text{Normale}(\theta, \sigma^2_2)$ et $\Theta \sim
    \text{Normale}(\mu, \sigma^2_1)$
  \item $S|\Theta = \theta \sim \text{Bernoulli}(\theta)$ et $\Theta \sim
    \text{Bêta}(a, b)$
  \item $S|\Theta = \theta \sim \text{Géométrique}(\theta)$ et $\Theta \sim
    \text{Bêta}(a, b)$
  \item[] $+$
  \item[] convolutions
    \begin{itemize}
    \item Gamma/gamma
    \item Binomiale/bêta
    \item Binomiale négative/bêta
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Conjuguée naturelle et famille exponentielle}

  \begin{itemize}
  \item En analyse bayésienne, \\
    \begin{center}
      $u(\theta|x_1, \dots, x_n)$ même famille que $u(\theta)$ \\
      $\Downarrow$ \\
      $u(\theta)$ et $f(x|\theta)$ sont des \alert{conjuguées
        naturelles}
    \end{center}
  \item Poisson, exponentielle, normale, Bernoulli et géométrique
    appartiennent toutes à la \alert{famille exponentielle univariée},
    c'est-à-dire que leur f.d.p.\ (ou f.m.p.) peut s'écrire sous la
    forme
    \begin{displaymath}
      f(x|\theta) = \frac{p(x) e^{-\theta x}}{q(\theta)}
    \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Modèle de Jewell}

  \begin{itemize}
  \item \citet{Jewell:exact:1974} démontre que
    \bigskip
    \begin{center}
      \begin{minipage}[c]{0.27\textwidth}
        \raggedright
        $f(x|\theta)$ dans \newline famille exponentielle
      \end{minipage}
      \hfill $+$ \hfill
      \begin{minipage}[c]{0.15\textwidth}
        \raggedright conjuguée naturelle
      \end{minipage}
      \hfill $\Rightarrow$ \hfill
      \begin{minipage}[c]{0.22\textwidth}
        \raggedright prime bayésienne linéaire
      \end{minipage}
    \end{center}
    \bigskip
  \item \citet{Goel:conjecture:1982} conjecture que ceci n'arrive
    qu'avec les membres de la famille exponentielle
    \begin{itemize}
    \item il ne peut le prouver;
    \item il ne peut non plus donner de contre-exemple.
    \end{itemize}
  \end{itemize}
\end{frame}

%%% Local Variables:
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
