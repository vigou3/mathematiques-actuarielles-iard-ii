%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Méthode de Bornhuetter--Ferguson}

\begin{frame}
  \frametitle{Présentation}

  Méthode présentée dans le célèbre article de
  \citet{Bornhuetter:1972}.

  \begin{itemize}

  \item Également entièrement déterministe au départ.
  \item Idée de base:
    \begin{enumerate}
    \item montant ultime espéré supposé connu
      \begin{equation*}
        \esp{C_{i, J}} = \mu_i
      \end{equation*}
    \item provision pour l'année d'accident $i$ correspond à la
      proportion du montant ultime qui «reste à venir»
      \begin{equation*}
        R_i = (1 - \beta_{I - i}) \mu_i
      \end{equation*}
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Hypothèses}

  Toujours tel que proposé par \citet{Wuthrich:reserving:2008}.

  \begin{enumerate}
  \item Les montants cumulatifs des sinistres encourus $C_{i, j}$
    d'années d'accidents différentes sont indépendants.
  \item Il existe des paramètres $\mu_1, \dots, \mu_I > 0$ et des
    facteurs $\beta_1, \dots, \beta_J > 0$ avec $\beta_J = 1$ tel que
    \begin{align*}
      \esp{C_{i, 1}} &= \beta_1 \mu_i \\
      \esp{C_{i, j + k}|C_{i, 1}, \dots, C_{i, j}} &=
      C_{i, j} + (\beta_{j + k} - \beta_j) \mu_i
    \end{align*}
    pour tout $i = 1, \dots, I$, $j = 1, \dots, J - 1$ et $k = 1,
    \dots, J - j$.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Conséquences des hypothèses}

  \begin{itemize}
  \item On remarque que
    \begin{align*}
      \esp{C_{i, 2}}
      &= \esp{\esp{C_{i, 2}|C_{i, 1}}} \\
      &= \esp{C_{i, 1} + (\beta_2 - \beta_1) \mu_i} \\
      &= \beta_1 \mu_i + (\beta_2 - \beta_1) \mu_i \\
      &= \beta_2 \mu_i, \\
      \intertext{d'où, par récurrence:}
      \esp{C_{i, j}} &= \beta_j \mu_i \\
      \esp{C_{i, J}} &= \mu_i.
    \end{align*}
  \item Montant cumulatif espéré dans une année est un pourcentage
    fixe (par année d'accident) du montant espéré ultime.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conséquences des hypothèses (suite)}

  \begin{itemize}
  \item Projection à l'\alert{ultime}:
    \begin{equation*}
      \esp{C_{i, J}|C_{i, 1}, \dots, C_{i, I - i + 1}} =
      C_{i, I - i + 1} + (1 - \beta_{I - i + 1}) \mu_i.
    \end{equation*}
  \item Exprimé sous forme de \alert{provision}:
    \begin{align*}
      \esp{R_i|C_{i, 1}, \dots, C_{i, I - i + 1}}
      &= \esp{C_{i, J} - C_{i, I - i + 1}|C_{i, 1}, \dots, C_{i, I - i + 1}} \\
      &= \esp{C_{i, J}|C_{i, 1}, \dots, C_{i, I - i + 1}} -
        C_{i, I - i + 1} \\
      &= (1 - \beta_{I - i + 1}) \mu_i.
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Estimateur de Bornhuetter--Ferguson}

  \begin{itemize}
  \item Estimateur de Bornhuetter--Ferguson de la projection des
    sinistres cumulatifs à l'ultime
    $\esp{C_{i, J}|C_{i, 1}, \dots, C_{i, I - i + 1}}$ pour
    $i = 1, \dots, I$:
    \begin{equation*}
      \hat{C}_{i, J}^{\text{BF}} =
      \hesp{C_{i, J}|C_{i, 1}, \dots, C_{i, I - i + 1}} =
      C_{i, I - i + 1} + (1 - \hat{\beta}_{I - i + 1}) \hat{\mu}_i.
    \end{equation*}
  \item Exprimé sous forme de provision:
    \begin{equation*}
      \hat{R}_i^{\text{BF}} = (1 - \hat{\beta}_{I - i + 1}) \hat{\mu}_i.
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Calcul en pratique}

  \begin{itemize}
  \item Nous devons déterminer des estimateurs appropriés pour les
    paramètres de la structure de développement
    $\hat{\beta}_1, \dots, \hat{\beta}_J$ et pour les sinistres
    ultimes espérés $\hat{\mu}_1, \dots, \hat{\mu}_I$.
  \item Nous pourrons réutiliser des idées de la méthode Chain-Ladder
    après avoir remarqué ceci:
    \begin{align*}
      \hat{R}_i^{\text{CL}}
      &= \hat{C}_{i, J}^{\text{CL}} - C_{i, J - i + 1} \\
      &= \hat{C}_{i, J}^{\text{CL}}
        \left(
        1 - \frac{C_{i, J - i + 1}}{\hat{C}_{i, J}^{\text{CL}}}
        \right) \\
      &= \hat{C}_{i, J}^{\text{CL}}
        \left(
        1 - \frac{C_{i, J - i + 1}}{C_{i, J - i + 1} \hat{\lambda}_{J - i + 1} \cdots \hat{\lambda}_{J - 1}}
        \right) \\
      &= \hat{C}_{i, J}^{\text{CL}}
        \left(
        1 - \frac{1}{\prod_{j = J - i + 1}^{J - 1} \hat{\lambda}_j}
        \right)
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Estimateurs des paramètres de développement}

  Nous allons utiliser
  \begin{align*}
    \hat{\beta}_j = \frac{1}{\prod_{k = j}^{J - 1} \hat{\lambda}_k} =
    \prod_{k = j}^{J - 1} \frac{1}{\hat{\lambda}_k}.
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Estimateurs des sinistres ultimes espérés}

  \begin{itemize}
  \item \alert{En principe}, n'importe quelle bonne estimation
    d'expert $\hat{\mu}_i$ de $\mu_i$ serait appropriée.
  \item \alert{En pratique}, les estimateurs sont calculés à partir
    des taux de sinistralité (\emph{loss ratios}, $\text{LR}$),
    supposés connus, et des primes acquises ($\text{PA}$) de chaque
    année d'accident:
    \begin{equation*}
      \hat{\mu}_i = \text{LR}_i \times \text{PA}_i.
    \end{equation*}
  \item Les taux de sinistralité globaux sont généralement assez bien
    connus par ligne d'affaires.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exemple}

  Données de l'exemple de Chain-Ladder, mais avec en plus les primes
  acquises et les taux de sinistralité par année d'accident.

  \begin{center}
    \begin{tabular}{crrrrrrr}
      \toprule
      & & & \multicolumn{5}{c}{Développement (âge)} \\
      Année
      & \multicolumn{1}{c}{$\text{PA}$}
        & \multicolumn{1}{c}{$\text{LR}$}
          & 1 & 2 & 3 & 4 & 5 \\
      \midrule
      1 & 330 & $0,60$ & 100 & 150 & 175 & 180 & 200 \\
      2 & 350 & $0,65$ & 110 & 168 & 192 & 205 \\
      3 & 365 & $0,70$ & 115 & 169 & 202 \\
      4 & 385 & $0,75$ & 125 & 185 \\
      5 & 400 & $0,80$ & 150 \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite)}

  Estimateurs des paramètres du modèle.
  \begin{align*}
    \hat{\beta}_5 &= 1
    & \hat{\mu}_1 &= \text{LR}_1 \text{PA}_1 = 198 \\
    \hat{\beta}_4 &= \frac{1}{\hat{\lambda}_4} = 0,900
    & \hat{\mu}_2 &= \text{LR}_2 \text{PA}_2 = 227,50 \\
    \hat{\beta}_3 &= \frac{1}{\hat{\lambda}_3 \hat{\lambda}_4} = 0,858
    & \hat{\mu}_3 &= \text{LR}_3 \text{PA}_3 = 255,50 \\
    \hat{\beta}_2 &= \frac{1}{\hat{\lambda}_2 \hat{\lambda}_3 \hat{\lambda}_4} = 0,734
    & \hat{\mu}_4 &= \text{LR}_4 \text{PA}_4 = 288,75 \\
    \hat{\beta}_1 &= \frac{1}{\hat{\lambda}_1 \hat{\lambda}_2 \hat{\lambda}_3 \hat{\lambda}_4} = 0,492
    & \hat{\mu}_5 &= \text{LR}_5 \text{PA}_5 = 320
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite)}

  Nous pouvons ensuite calculer directement les provisions (plus
  simple) ou encore les estimateurs de Bornhuetter--Ferguson des
  sinistres cumulatifs ultimes.
  \begin{align*}
    \hat{R}_1 &= (1 - \hat{\beta}_5) \hat{\mu}_1 = 0
    & \hat{C}_{1, 5}^{\text{BF}} &= C_{1, 5} = 200 \\
    \hat{R}_2 &= (1 - \hat{\beta}_4) \hat{\mu}_2 = 22,75
    & \hat{C}_{2, 5}^{\text{BF}} &= C_{2, 4} +  (1 - \hat{\beta}_4) \hat{\mu}_2 = 227,75 \\
    \hat{R}_3 &= (1 - \hat{\beta}_3) \hat{\mu}_3 = 36,30
    & \hat{C}_{3, 5}^{\text{BF}} &= C_{3, 3} +  (1 - \hat{\beta}_3) \hat{\mu}_3 = 238,30 \\
    \hat{R}_4 &= (1 - \hat{\beta}_2) \hat{\mu}_4 = 76,73
    & \hat{C}_{4, 5}^{\text{BF}} &= C_{4, 2} +  (1 - \hat{\beta}_2) \hat{\mu}_4 = 261,73 \\
    \hat{R}_5 &= (1 - \hat{\beta}_1) \hat{\mu}_5 = 162,65
    & \hat{C}_{5, 5}^{\text{BF}} &= C_{5, 1} +  (1 - \hat{\beta}_1) \hat{\mu}_5 = 312,65
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite et fin)}

  Sommaire des résultats en un seul tableau.

  \begin{center}
    \footnotesize
    \begin{tabular}{crrrrrrrrrr}
      \toprule
      & & & \multicolumn{5}{c}{Développement (âge)} \\
      Année
      & \multicolumn{1}{c}{$\text{PA}$}
        & \multicolumn{1}{c}{$\text{LR}$}
          & 1 & 2 & 3 & 4 & 5
                  & $\hat{\mu}_i$ & $1 - \hat{\beta}_i$
                          & Provision \\
      \midrule
      1 & 330 & $0,60$ & 100 & 150 & 175 & 180 & 200    & 198,00 & 0 & 0 \\
      2 & 350 & $0,65$ & 110 & 168 & 192 & 205 & 227,75 & 227,50 & 0,100 &  22,75 \\
      3 & 365 & $0,70$ & 115 & 169 & 202 &     & 238,30 & 255,50 & 0,142 &  36,30 \\
      4 & 385 & $0,75$ & 125 & 185 &     &     & 261,73 & 288,75 & 0,266 &  76,73 \\
      5 & 400 & $0,80$ & 150 &     &     &     & 312,65 & 320,00 & 0.508 & 162,65 \\
      \midrule
      $\hat{\lambda}_j$ & & & $1,493$ & $1,168$ & $1,049$ & $1,111$ \\
      \midrule
      TOTAL & & & & & & & & & & 298,44 \\
      \bottomrule
    \end{tabular}
  \end{center}

  \medskip
  \gotoR{bornhuetter-ferguson.R}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
