%%% Copyright (C)  Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% http://creativecommons.org/licenses/by-sa/4.0/

\section{Modèle de Bühlmann--Straub}

\begin{frame}
  \frametitle{Modèle pour exposition au risque variable}

  \begin{itemize}
  \item \citet{BuhlmannStraub:1970} associent un poids $w_{it}$ à
    chaque donnée, qui sera maintenant notée $X_{it}$
  \item Schématiquement:
  \end{itemize}
  \begin{center}
    \small
    \begin{tabular}{*{11}{c}}
      \toprule
      Niveau &
      \multicolumn{5}{c}{Observations} &
      \multicolumn{5}{c}{Poids} \\
      risque   & $1$ & $\dots$ & $t$ & $\dots$ & $n$
      & $1$ & $\dots$ & $t$ & $\dots$ & $n$ \\
      \midrule
      $\Theta_1$ & $X_{11}$ & $\dots$ & $X_{1t}$ & $\dots$ & $X_{1n}$
      & $w_{11}$ & $\dots$ & $w_{1t}$ & $\dots$ & $w_{1n}$ \\
      $\vdots$ & $\vdots$ & & & & $\vdots$
      & $\vdots$ & & & & $\vdots$ \\
      $\Theta_i$ & $X_{i1}$ & $\dots$ & $X_{it}$ & $\dots$ & $X_{in}$
      & $w_{i1}$ & $\dots$ & $w_{it}$ & $\dots$ & $w_{in}$ \\
      $\vdots$ & $\vdots$ & & & & $\vdots$
      & $\vdots$ & & & & $\vdots$ \\
      $\Theta_I$ & $X_{I1}$ & $\dots$ & $X_{It}$ & $\dots$ & $X_{In}$
      & $w_{I1}$ & $\dots$ & $w_{It}$ & $\dots$ & $w_{In}$ \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Hypothèses}

  \begin{enumerate}[({BS}1)]
  \item Les contrats $(\Theta_i, \mat{X}_i)$, $i = 1, \dots, I$ sont
    indépendants, les variables aléatoires $\Theta_1, \dots, \Theta_I$
    sont identiquement distribuées et les variables aléatoires
    $X_{it}$ ont une variance finie
  \item Les variables aléatoires $X_{it}$, sont telles que
    \begin{align*}
      \esp{X_{it}|\Theta_i} &= \mu(\Theta_i)
      \quad i = 1, \dots, I \\
      \Cov(X_{it}, X_{iu}|\Theta_i) &= \delta_{tu}
      \frac{\sigma^2(\Theta_i)}{w_{it}}, \quad t, u = 1, \dots, n
    \end{align*}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Remarques importantes}
  \begin{enumerate}
  \item Nous avons
    \begin{displaymath}
      \var{X_{it}|\Theta_i} = \frac{\sigma^2(\Theta_i)}{w_{it}}
    \end{displaymath}
  \item Implique que les variables $X_{it}$ doivent être des
    \alert{ratios}
  \item Souvent,
    \begin{equation*}
      X_{it} = \frac{S_{it}}{w_{it}}
    \end{equation*}
  \item Nous avons toujours $s^2 = \esp{\sigma^2(\Theta)}$, mais
    $s^2 \neq \esp{\var{X_{it}|\Theta}}$
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Notation}

  Le modèle de Bühlmann-Straub fait appel à des \alert{moyennes
    pondérées}.

  \begin{align*}
    X_{iw}
    &= \sum_{t = 1}^n \frac{w_{it}}{w_{i\pt}}\, X_{it} &
    w_{i\pt}
    &= \sum_{t = 1}^n w_{it} \\[12pt]
    X_{ww}
    &= \sum_{i = 1}^I \frac{w_{i\pt}}{w_{\pt\pt}}\, X_{iw} &
    w_{\pt\pt}
    &= \sum_{t = 1}^n w_{i\pt} \\[12pt]
    X_{zw}
    &= \sum_{i = 1}^I \frac{z_i}{z_\pt}\, X_{iw} &
    z_\pt
    &= \sum_{i = 1}^I z_i
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Relations de variance et de covariance}

  Le truc, c'est de les calculer dans cet ordre.
  \begin{align*}
    \Cov(X_{it}, X_{ju})
    &= \delta_{ij} \left( a + \delta_{tu} \frac{s^2}{w_{it}} \right)
      \visible<2->{&\alert{\rightarrow} &&
              \var{X_{it}} &= \Cov(X_{it}, X_{it})} \\
    &\alert{\downarrow}
      \visible<2->{&\alert{\swarrow}} \\
    \Cov(X_{it}, X_{iw})
    &= \sum_{u = 1}^n \frac{w_{iu}}{w_{i\pt}}
      \Cov(X_{it}, X_{iu})
      \visible<2->{&\alert{\rightarrow} &&
                                        \var{X_{iw}}
                           &= \Cov(X_{iw}, X_{iw})
                             = \sum_{t = 1}^n \frac{w_{it}}{w_{i\pt}}
                             \Cov(X_{it}, X_{iw})} \\
    &\alert{\downarrow}
      \visible<2->{&\alert{\swarrow}} \\
    \Cov(X_{iw}, X_{ww})
    &= \sum_{j = 1}^I \frac{w_{i\pt}}{w_{\pt\pt}}
      \Cov(X_{iw}, X_{jw})
      \visible<2->{&\alert{\rightarrow} &&
                                        \var{X_{ww}}
                           &= \Cov(X_{ww}, X_{ww})
                             = \sum_{i = 1}^I \frac{w_{i\pt}}{w_{\pt\pt}}
                             \Cov(X_{iw}, X_{ww})}
  \end{align*}
\end{frame}


\begin{frame}
  \frametitle{Prévision}

  La meilleure approximation linéaire \alert{non homogène} de la prime
  de risque $\mu(\Theta_i)$ --- ou de $X_{i, n + 1}$ --- est
  \begin{equation*}
    \pi_{i, n + 1}^{\text{BS}} = z_i X_{iw} + (1 - z_i) m
  \end{equation*}
  où
  \begin{align*}
    z_i
    &= \frac{w_{i\pt}}{w_{i\pt} + K}, \quad
    K = \frac{s^2}{a}.
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Estimation de la moyenne collective}
  \begin{itemize}[<+->]
  \item Estimateur intuitif:
    \begin{displaymath}
      X_{ww} = \sum_{i = 1}^I \frac{w_{i\pt}}{w_{\pt\pt}}\, X_{iw}.
    \end{displaymath}
  \item Meilleur choix (plus faible variance):
    \begin{displaymath}
      \hat{m} = X_{zw} = \sum_{i = 1}^I \frac{z_i}{z_\pt}\, X_{iw}
    \end{displaymath}
  \item Calculer $\displaystyle \lim_{z_i \rightarrow 0} X_{zw}$ pour
    savoir ce qui se passe si $z_1 = \dots = z_I = 0$ ($s^2 = \infty$
    ou $a = 0$)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Estimation de la variance intra}

  Par analogie avec l'estimateur du modèle de Bühlmann, un estimateur
  sans biais de $s^2$ est
  \begin{displaymath}
    \hat{s}^2 = \frac{1}{I(n - 1)} \sum_{i = 1}^I \sum_{t = 1}^n
    w_{it} (X_{it} - X_{iw})^2
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Estimation de la variance inter}
  \begin{itemize}[<+->]
  \item Estimateur intuitif rendu sans biais:
    \begin{displaymath}
      \hat{a} = \frac{w_{\pt\pt}}{w_{\pt\pt}^2 - \sum_{i=1}^I
        w_{i\pt}^2}
      \left(
        \sum_{i=1}^I w_{i\pt} (X_{iw} - X_{ww})^2 - (I - 1) \hat{s}^2
      \right)
    \end{displaymath}
  \item Problème: peut être négatif
  \item \alert{Pseudo-estimateur} de Bichsel--Straub sans biais et
    toujours positif:
    \begin{displaymath}
      \tilde{a} = \frac{1}{I - 1} \sum_{i = 1}^I z_i (X_{iw} - X_{zw})^2
    \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Données manquantes}

  Fréquent en pratique.
  \begin{itemize}
  \item Nombre d'observations non identique d'un contrat à l'autre
  \item $X_{it}$ pour $t = 1, \dots, n_i$
  \item Seule formule affectée:
    \begin{displaymath}
      \hat{s}^2 = \frac{1}{\sum_{i = 1}^I (n_i - 1)}
      \sum_{i = 1}^I \sum_{t = 1}^n w_{it} (X_{it} - X_{iw})^2
    \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exemple numérique}

  \begin{itemize}
  \item Données de Hachemeister (1975)
  \item $X_{it}$: montants de sinistres \alert{moyens}, responsabilité
    civile en assurance automobile, juillet 1970--juin 1973 pour cinq
    états américains
  \item $w_{it}$: nombre total de sinistres dans chaque période pour
    chaque état
  \item $I = 5$ contrats et $n = 12$ périodes d'expérience
  \end{itemize}
  \pause

  \gotoR{buhlmann-straub.R}
\end{frame}

%%% Local Variables:
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
