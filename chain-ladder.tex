%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Méthode Chain-Ladder}

\begin{frame}
  \frametitle{Présentation}

  La méthode Chain-Ladder est la plus ancienne méthode de
  provisionnement.

  \begin{itemize}
  \item Toujours aussi la plus employée en pratique.
  \item Entièrement déterministe à l'origine
    \begin{itemize}
    \item suffisant pour obtenir une estimation de la provision
    \end{itemize}
  \item Possible de fournir un cadre stochastique (plusieurs façons!)
    \begin{itemize}
    \item permet de mesurer le risque associé à la prévision
    \end{itemize}
  \item Idée de base:
    \begin{equation*}
      C_{i, j} = C_{i, j - 1} \lambda_{j - 1}
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Hypothèses (approche non paramétrique)}

  Nous utilisons l'approche de \citet{Wuthrich:reserving:2008}.

  \begin{enumerate}
  \item Les montants cumulatifs des sinistres encourus $C_{i, j}$
    d'années d'accidents différentes sont indépendants.
  \item Il existe des facteurs de déroulement
    $\lambda_1, \dots, \lambda_{J - 1} > 0$ tel que
    \begin{equation*}
      \esp{C_{i, j}|C_{i, 1}, \dots, C_{i, j - 1}} =
      \esp{C_{i, j}|C_{i, j - 1}} = \lambda_{j - 1} C_{i, j - 1}
    \end{equation*}
    pour tout $i = 1, \dots, I$ et $j = 2, \dots, J$.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Conséquences des hypothèses}

  \begin{itemize}
  \item Seules les observations de l'année d'accident $i$ servent dans
    la projection de cette année:
    \begin{equation*}
      \esp{C_{i, j}|\mathcal{D}_I} =
      \esp{C_{i, j}|C_{i, 1}, \dots, C_{i, j - 1}}
    \end{equation*}
  \item Seul le \alert{dernier} montant cumulatif des sinistres
    disponible pour une année d'accident donnée est utilisé dans la
    projection (pensez chaine de Markov)
  \item Par récurrence, la projection à l'\alert{ultime} ($j = J$) est:
    \begin{align*}
      \esp{C_{i, J}|C_{i, 1}, \dots, C_{i, I - i + 1}}
      &= \lambda_{J - 1} \esp{C_{i, J - 1}|C_{i, 1}, \dots, C_{i, I - i + 1}} \\
      &= \lambda_{J - 1} \lambda_{J - 2} \esp{C_{i, J - 2}|C_{i, 1}, \dots, C_{i,
        I - i + 1}} \\
      &\; \vdots \\
      &= \lambda_{J - 1} \lambda_{J - 2} \cdots \lambda_{I - i + 1}
        \esp{C_{i, I - i + 2}|C_{i, 1}, \dots, C_{i, I - i + 1}} \\
      &= C_{i, I - i + 1} \lambda_{I - i + 1} \cdots \lambda_{J - 1}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Estimateur Chain-Ladder}

  En pratique, les facteurs de déroulement
  $\lambda_1, \dots, \lambda_{J - 1}$ sont inconnus.

  \begin{itemize}
  \item Estimateur du facteur de déroulement $\lambda_j$,
    $j = 1, \dots, J - 1$:
    \begin{align*}
      \hat{\lambda}_j
      &= \frac{\sum_{i = 1}^{I - j} C_{i, j + 1}}{%
        \sum_{i = 1}^{I - j} C_{i, j}} \\
      &= \sum_{i = 1}^{I - j} \frac{C_{i, j}}{C_{\pt, j}}
        \frac{C_{i, j + 1}}{C_{i, j}}, \quad
        C_{\pt, j} = \sum_{k = 1}^{I - j} C_{k, j}
    \end{align*}
    (Moyenne pondérée des facteurs de déroulement par année d'accident)
  \item Estimateur Chain-Ladder de
    $\esp{C_{i, j}|C_{i, 1}, \dots, C_{i, j - 1}}$ pour $i + j - 1 > I$
    (partie inférieure du triangle):
    \begin{equation*}
      \hat{C}_{i, j}^{\text{CL}} =
      \hesp{C_{i, j}|C_{i, j - 1}} =
      C_{i, I - j + 1} \hat{\lambda}_{I - i} \cdots \hat{\lambda}_{j - 1}.
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Provisions}

  Les provisions correspondent aux montants encore à payer pour les
  sinistres.

  \begin{itemize}
  \item Provision Chain-Ladder pour l'année d'accident $i$ est la
    différence entre les sinistres \alert{ultimes} et les sinistres
    \alert{payés} en date d'évaluation:
    \begin{equation*}
      \hat{R}_i^{\text{CL}} = \hat{C}_{i, J}^{\text{CL}} - C_{i, J - i + 1}.
    \end{equation*}
  \item Provision totale est la somme des provisions par année
    d'accident:
    \begin{align*}
      \hat{R}^{\text{CL}}
      &= \sum_{i = 1}^I \hat{R}_i^{\text{CL}} \\
      &= \sum_{i = 1}^I (\hat{C}_{i, J}^{\text{CL}} - C_{i, J - i + 1}).
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exemple}

  \begin{columns}
    \begin{column}{0.5\textwidth}
      Données
      \begin{center}
        \begin{tabular}{crrrrr}
          \toprule
          & \multicolumn{5}{c}{Développement (âge)} \\
          Année & 1 & 2 & 3 & 4 & 5 \\
          \midrule
          1 & 100 & 150 & 175 & 180 & 200 \\
          2 & 110 & 168 & 192 & 205 \\
          3 & 115 & 169 & 202 \\
          4 & 125 & 185 \\
          5 & 150 \\
          \bottomrule
        \end{tabular}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      Estimateurs des facteurs de déroulement
      \begin{align*}
        \hat{\lambda}_4 &= \frac{200}{180} = 1,111 \\[6pt]
        \hat{\lambda}_3 &= \frac{180+205}{175+192} = 1,049 \\[6pt]
        \hat{\lambda}_2 &= \frac{175+192+202}{150+168+169} = 1,168 \\[6pt]
        \hat{\lambda}_1 &= \frac{150+168+169+185}{100+110+115+125} = 1,493
      \end{align*}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite)}

  \begin{columns}
    \begin{column}{0.35\textwidth}
      \begin{align*}
        \hat{C}_{2,5}^{\text{CL}}
        &= C_{2, 4} \hat{\lambda}_4 \\
        &= 205 (1,111) \\
        &= 227,78 \\
        \hat{C}_{3,4}^{\text{CL}}
        &= C_{3, 3} \hat{\lambda}_3 \\
        &= 202 (1,049) \\
        &= 211,91 \\
        \hat{C}_{3,5}^{\text{CL}}
        &= \hat{C}_{3, 4}^{\text{CL}} \hat{\lambda}_4 = C_{3, 3} \hat{\lambda}_3 \hat{\lambda}_4 \\
        &= 202 (1,049)(1,111) \\
        &= 235,45 \\
        \hat{C}_{4,5}^{\text{CL}}
        &= C_{4, 2} \hat{\lambda}_2 \hat{\lambda}_3 \hat{\lambda}_4 \\
        &= 185 (1,169)(1,049)(1,111) \\
        &= 251,95
      \end{align*}
    \end{column}
    \begin{column}{0.65\textwidth}
      \begin{center}
        \begin{tabular}{crrrrr}
          \toprule
          & \multicolumn{5}{c}{Développement (âge)} \\
          Année & 1 & 2 & 3 & 4 & 5 \\
          \midrule
          1 & 100 & 150 & 175 & 180 & 200 \\
          2 & 110 & 168 & 192 & 205 & \alert{227,78} \\
          3 & 115 & 169 & 202 & \alert{211,91} & \alert{235,45} \\
          4 & 125 & 185 & \alert{216,15} & \alert{226,75} & \alert{251,95} \\
          5 & 150 & \alert{224,00} & \alert{261,72} & \alert{274,55} & \alert{305,06} \\
          \bottomrule
        \end{tabular}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite)}

  \begin{itemize}
  \item Provisions par année d'accident:
    \begin{align*}
      \hat{R}_1^{\text{CL}}
      &= C_{1, 5} - C_{1, 5} = 0 \\
      \hat{R}_2^{\text{CL}}
      &= \hat{C}_{2, 5}^{\text{CL}} - C_{2, 4} = 22,78 \\
      \hat{R}_3^{\text{CL}}
      &= \hat{C}_{3, 5}^{\text{CL}} - C_{3, 3} = 33,45 \\
      \hat{R}_4^{\text{CL}}
      &= \hat{C}_{4, 5}^{\text{CL}} - C_{4, 2} = 66,95 \\
      \hat{R}_5^{\text{CL}}
      &= \hat{C}_{5, 5}^{\text{CL}} - C_{5, 1} = 155,06
    \end{align*}
  \item Provision totale:
    \begin{equation*}
      \hat{R}^{\text{CL}}
      = 22,78 + 33,45 + 66,95 + 155,06 = 278,24.
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite et fin)}

  Sommaire des résultats en un seul tableau.

  \begin{center}
    \begin{tabular}{crrrrrr}
      \toprule
      & \multicolumn{5}{c}{Développement (âge)} \\
      Année & 1 & 2 & 3 & 4 & 5 & Provision \\
      \midrule
      1 & 100 & 150    & 175    & 180    & 200    &   0,00 \\
      2 & 110 & 168    & 192    & 205    & 227,78 &  22,78 \\
      3 & 115 & 169    & 202    & 211,91 & 235,45 &  33,45 \\
      4 & 125 & 185    & 216,15 & 226,75 & 251,95 &  66,95 \\
      5 & 150 & 224,00 & 261,72 & 274,55 & 305,06 & 155,06 \\
      \midrule
      $\hat{\lambda}_j$ & $1,493$ & $1,168$ & $1,049$ & $1,111$ \\
      \midrule
      TOTAL & & & & & & 278,24 \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Remarques}

  \begin{itemize}
  \item La méthode Chain-Ladder suppose que l'âge des sinistres est la
    seule variable explicative du développement.
  \item La méthode Chain-Ladder suppose également que le facteur de
    déroulement n'est pas fonction de l'année d'accident,
    c'est-à-dire que $\lambda_{i, j} = \lambda_j$.
  \item La provision $\hat{R}_n$ de la plus récente année d'accident
    est sujette à une forte incertitude.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Calcul numérique}

  Le paquetage R \pkg{ChainLadder} \citep{Rpackage:ChainLadder} permet
  de calculer des provisions en assurance IARD par différentes
  méthodes, dont Chain-Ladder.

  \medskip
  \gotoR{chain-ladder.R}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
