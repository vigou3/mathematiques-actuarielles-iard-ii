%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section[Introduction à la tarification basée sur l'expérience]{Introduction}

\begin{frame}
  \frametitle{Contexte}

  \begin{itemize}
  \item Portefeuille d'assurance composé de dix contrats
  \item Contrats à priori considérés équivalents
  \item Hypothèses:
    \begin{itemize}
    \item au plus un sinistre par année
    \item montant de ce sinistre est 1
    \item prime collective de 0,20: en moyenne deux assurés sur 10
      ont un sinistre au cours d'une année
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Situation après une année}

  \begin{center}
    \begin{tabularx}{\tablewidth}{c*{10}{Y}}
      & \multicolumn{10}{c}{Contrat} \\
      \cmidrule{2-11}
      Année & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
      \midrule
      1 &   &   &   &   &   &   &   &   & 1 &   \\
    \end{tabularx}
  \end{center}

  \begin{itemize}
  \item Montant de sinistre moyen par contrat = 1/10 = 0,10
  \item Prime collective \alert{peut-être} trop élevée
  \item Trop peu de données pour tirer une conclusion
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Situation après deux années}

  \begin{center}
    \begin{tabularx}{\tablewidth}{c*{10}{Y}}
      & \multicolumn{10}{c}{Contrat} \\
      \cmidrule{2-11}
      Année & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
      \midrule
      1 &   &   &   &   &   &   &   &   & 1 &   \\
      \midrule
      2 & 1 & 1 &   &   &   &   &   &   & 1 &   \\
    \end{tabularx}
  \end{center}

  \begin{itemize}
  \item Montant de sinistre moyen par contrat = 4/20 = 0,20
  \item Prime collective adéquate
  \item Contrat 9 a déjà deux sinistres
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \frametitle{Situation après dix années}

  \begin{center}
    \small
    \begin{tabularx}{\tablewidth}{c*{10}{Y}}
      & \multicolumn{10}{c}{Contrat} \\
      \cmidrule{2-11}
      Année & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
      \specialrule{\lightrulewidth}{\aboverulesep}{0.1em}
      1 &   &   &   &   &   &   &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      2 & 1 & 1 & 1 &   &   &   &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      3 & 1 &   &   &   &   &   &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      4 &   &   & 1 &   &   &   &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      5 &   &   &   &   &   &   &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      6 &   & 1 &   &   &   &   &   &   &   &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      7 & 1 & 1 &   & 1 & 1 &   &   &   &   &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      8 & 1 &   &   & 1 &   & 1 &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      9 & 1 &   &   &   & 1 &   &   &   &   &   \\
      \specialrule{\lightrulewidth}{0.1em}{0.1em}
      10 & 1 &   &   &   &   &   &   &   & 1 &   \\
      \specialrule{\lightrulewidth}{0.1em}{\belowrulesep}
      $\bar{S}_i$ & 0,6 & 0,3 & 0,2 & 0,2 & 0,2 & 0,1 & 0 & 0 & 0,7 & 0 \\
      \midrule
      $\bar{S}$ & \multicolumn{10}{c}{0,23} \\
    \end{tabularx}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Situation après dix années}

  \begin{itemize}
  \item Montant de sinistre moyen par contrat = 23/100 = 0,23
  \item Prime collective raisonnablement adéquate
  \item Contrat 9 plus risqué
  \item Contrats 7, 8 et 10 n'ont eu aucun sinistre
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Conclusions}

  \begin{itemize}
  \item Prime collective globalement \alert{adéquate}, mais pas
    \alert{équitable}
  \item Le portefeuille est \alert{hétérogène}
  \item Besoin d'une technique de tarification basée sur l'expérience
    (\emph{experience rating}) pour adéquatement distribuer les primes
    entre les assurés
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \centering

  Crédibilité est \alert{une} technique \\
  de tarification basée sur l'expérience.
\end{frame}

\begin{frame}
  \frametitle{Deux grandes branches de la théorie de la crédibilité}

  \begin{enumerate}
  \item Credibilité de \alert{stabilité}, ou américaine, \emph{limited
      fluctuations}
    \begin{itemize}
    \item[] Assureur tient compte de l'expérience individuelle seulement si
      elle est stable dans le temps
    \end{itemize}
  \item Crédibilité de \alert{précision}, ou européenne, \emph{greatest
      accuracy}
    \begin{itemize}
    \item Assureur tient compte de l'expérience individuelle de
      façon à obtenir la meilleure prime
    \end{itemize}
  \end{enumerate}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% coding: utf-8
%%% End:
