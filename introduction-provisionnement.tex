%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section[Introduction au provisionnement en assurance IARD]{Introduction}

\begin{frame}
  \frametitle{Définition}

  L'Autorité des marchés financiers (AMF) définit ainsi les provisions
  et réserves en assurance IARD:
  \begin{quote}
    Processus d'évaluation du montant total nécessaire pour
    acquitter tous les paiements futurs associés aux sinistres déjà
    survenus en date d'évaluation (ex. au 31 décembre).
  \end{quote}
\end{frame}

\begin{frame}
  \frametitle{Importance des provisions}

  Les provisions pour sinistres représentent environ 75~\% du passif
  total des assureurs de dommages.

  \begin{itemize}
  \item Provisions sous-évaluées
    \begin{itemize}
    \item santé financière de la compagnie surévaluée
    \item compagnie exposée au risque de défaut sur ses paiements
      futurs
    \item assureur exposé à la ruine
    \end{itemize}
  \item Provisions surévaluées
    \begin{itemize}
    \item dépenses plus élevées
    \item profit diminué
    \item impôts diminués
    \item surplus diminué
    \item valeur moindre de la compagnie
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Assurance vie}

  \begin{itemize}
  \item Long terme
  \item Prestations connues
  \item \alert{Moment} ou \alert{durée} des prestations inconnus
  \item Primes à percevoir

    \begin{equation*}
      \text{provision} = \text{v.a.\ des prestations futures} -
      \text{v.a.\ des primes futures}
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Assurance IARD}

  \begin{itemize}
  \item Contrats de courte durée
  \item Prestations inconnues (\alert{montant} et \alert{moment})
  \item Prestations même si police plus en vigueur
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Assurance IARD (suite)}

  Exemple d'historique d'une police d'assurance IARD:
  \begin{center}
    \setlength{\unitlength}{2mm}
    \begin{picture}(65,12)
      \setlength{\fboxsep}{1.5pt}
      \put(0,0){\colorbox{lightgray}{\makebox(12.5,1.5){}}}
      \thicklines
      \put(0,0.75){\vector(1,0){65}}
      \put( 6,0){\line(0,1){1.5}}
      \put(15,0){\line(0,1){1.5}}
      \put(22,0){\line(0,1){1.5}}
      \put(24,0){\line(0,1){1.5}}
      \put(27,0){\line(0,1){1.5}}
      \put(35,0){\line(0,1){1.5}}
      \put(45,0){\line(0,1){1.5}}
      \put(51,0){\line(0,1){1.5}}
      \put(54,0){\line(0,1){1.5}}
      \put(60,0){\line(0,1){1.5}}

      \thinlines
      \put( 6,9){\vector(0,-1){6}}
      \put(15,9){\vector(0,-1){6}}
      \put(22,7){\vector(0,-1){4}}
      \put(24,7){\vector(0,-1){4}}
      \put(27,7){\vector(0,-1){4}}
      \put(35,9){\vector(0,-1){6}}
      \put(45,9){\vector(0,-1){6}}
      \put(51,7){\vector(0,-1){4}}
      \put(54,7){\vector(0,-1){4}}
      \put(60,9){\vector(0,-1){6}}

      \small
      \put(0.1,-1.5){\makebox(12.5,0){police en vigueur}}
      \put( 6,10.5){\makebox(0,0){sinistre}}
      \put(15,10.5){\makebox(0,0){déclaration}}
      \put(24.5,8.5){\makebox(0,0){paiements}}
      \put(35,10.5){\makebox(0,0){fermeture}}
      \put(45,10.5){\makebox(0,0){réouverture}}
      \put(52.5,8.5){\makebox(0,0){paiements}}
      \put(60,10.5){\makebox(0,0){fermeture}}
    \end{picture}
  \end{center}

  \pause
  \begin{align*}
    \text{provision} = \text{provisions totales pour sinistres }
    &\text{\alert{subis mais non déclarés (SMND)}} - 0 \\
    &\quad\downarrow \\
    & \text{\emph{Incurred But Not Reported} (IBNR)}
  \end{align*}
\end{frame}

\begin{frame}[standout]
  Facteur clé du calcul des provisions \\
  en assurance de dommages

  \alert{Modélisation du développement des sinistres}
\end{frame}

\begin{frame}
  \frametitle{Un peu de terminologie}

  \begin{itemize}
  \item \textbf{Dossier de sinistre} (\emph{Claim file}) \par
    Dossier ouvert par l'assureur dès qu'un sinistre est déclaré;
    contient plusieurs informations sur la réclamation
    \begin{itemize}
    \item date du sinistre
    \item date de la réclamation
    \item montant et date de chaque paiement
    \item informations qualitatives
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Un peu de terminologie (suite)}

  \begin{itemize}
  \item \textbf{Provisions automatiques} (\emph{Case reserves}) \par
    Estimation des coûts totaux associés à un dossier de sinistre

  \item \textbf{Sinistres subis mais non déclarés (SMND)} (\emph{Incurred but
      not reported}, IBNR) \par
    Quatre composantes:
    \begin{enumerate}
    \item provisions pour le développement des sinistres déjà ouverts
    \item provisions pour dossiers fermés pouvant rouvrir
    \item provisions pour sinistres encourus, mais non déclarés
    \item provisions pour les sinistres rapportés, mais non codifiés
      dans le système informatique
    \end{enumerate}

  \item \textbf{Provisions totales} \par
    Somme des provisions automatiques et des provisions SMND (IBNR)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Un peu de terminologie (suite)}

  \begin{itemize}
  \item \textbf{Développement} \par
    Changement dans le temps de la somme des paiements faits par
    l'assureur pour tous ses assurés

  \item \textbf{Facteur de développement des sinistres} (\emph{Loss
      development factor}) \par
    \begin{equation*}
      \text{LDF}_j = \lambda_j = \frac{\text{Paiements totaux effectués à } t = j + 1}{%
        \text{Paiements totaux effectués à } t = j}
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Données pour le provisionnement en assurance IARD}

  Au 31 décembre 2016.

  \begin{center}
    \begin{tabular}{crrrrr}
      \toprule
      Année & \multicolumn{5}{c}{Développement (âge)} \\
      accident& 12 mois & 24 mois & 36 mois & 48 mois & 60 mois \\
      \midrule
      2012 & \nombre{5946975} & \nombre{9668212} & \nombre{10563929} & \nombre{10771690} & \nombre{10978394} \\
      2013 & \nombre{6346756} & \nombre{9593162} & \nombre{10316383} & \nombre{10468180} \\
      2014 & \nombre{6269090} & \nombre{9245313} & \nombre{10092366} \\
      2015 & \nombre{5863015} & \nombre{8546239} \\
      2016 & \nombre{5778885} \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Problème de provisionnement}

  Projeter les valeurs dans la partie inférieure du triangle.

  \begin{center}
    \begin{tabular}{crrrrr}
      \toprule
      Année & \multicolumn{5}{c}{Développement (âge)} \\
      accident& 12 mois & 24 mois & 36 mois & 48 mois & 60 mois \\
      \midrule
      2012 & \nombre{5946975} & \nombre{9668212} & \nombre{10563929} & \nombre{10771690} & \nombre{10978394} \\
      2013 & \nombre{6346756} & \nombre{9593162} & \nombre{10316383} & \nombre{10468180} & \textbf{*} \\
      2014 & \nombre{6269090} & \nombre{9245313} & \nombre{10092366} & \textbf{*} & \textbf{*} \\
      2015 & \nombre{5863015} & \nombre{8546239} & \textbf{*} & \textbf{*} & \textbf{*} \\
      2016 & \nombre{5778885} & \textbf{*} & \textbf{*} & \textbf{*} & \textbf{*} \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Notation générale}

  $C_{i, j}$ est le montant cumulatif des sinistres encourus à l'âge
  $j = 1, \dots, J$ pour l'année d'accident $i = 1, \dots, I$.

  Sans perte de généralité, nous allons supposer que $I = J$.

  \begin{center}
    \begin{tabular}{*{6}{c}}
      \toprule
      Année & \multicolumn{5}{c}{Développement (âge)} \\
      accident & $1$ & $\cdots$ & $j$ & $\cdots$ & $J$ \\
      \midrule
      $1$ & $C_{1, 1}$ & $\cdots$ & $C_{1, j}$ & $\cdots$ & $C_{1, J}$ \\
      $\vdots$ \\
      $i$ & $C_{i, 1}$ & $\cdots$ & $C_{i, j}$ \\
      $\vdots$ \\
      $I$ & $C_{I, 1}$ \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Avertissement}

  La notation porte facilement à confusion.
  \begin{itemize}
  \item Les observations de la \alert{diagonale} du triangle de
    développement sont
    \begin{equation*}
      C_{i, I - i + 1}, \quad i = 1, \dots, I
    \end{equation*}
  \item L'ensemble des observations dans la \alert{partie supérieure}
    du triangle de développement est
    \begin{equation*}
      \mathcal{D}_I = \{C_{i, j}; i + j - 1 \leq I, j \leq J\}
    \end{equation*}
  \end{itemize}
\end{frame}

%%% Local Variables:
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
