%%% Copyright (C) 2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Laboratoire secret}

% \begin{frame}[standout]
%   Laboratoire secret
% \end{frame}

\begin{frame}
  \frametitle{Pyramide de la communication en science des données}
  \centering

  \smaller
  \setlength{\unitlength}{7mm}
  \begin{picture}(12,9)
    \thicklines
    {%
      \color{OIGold}
      \polygon*(0,0)  (0.9,1.35)(11.1,1.35)(12,0)
      \color{OISkyBlue}
      \polygon*(1,1.5)(1.9,2.85)(10.1,2.85)(11,1.5)
      \color{OIGreen}
      \polygon*(2,3.0)(2.9,4.35)( 9.1,4.35)(10,3.0)
      \color{OIPink}
      \polygon*(3,4.5)(3.9,5.85)( 8.1,5.85)( 9,4.5)
      \color{OIBlue}
      \polygon*(4,6.0)(4.9,7.35)( 7.1,7.35)( 8,6.0)
      \color{OIRed}
      \polygon*(5,7.5)(6,9)(7,7.5)
    }

    \polygon(0,0)  (0.9,1.35)(11.1,1.35)(12,0)
    \polygon(1,1.5)(1.9,2.85)(10.1,2.85)(11,1.5)
    \polygon(2,3.0)(2.9,4.35)( 9.1,4.35)(10,3.0)
    \polygon(3,4.5)(3.9,5.85)( 8.1,5.85)( 9,4.5)
    \polygon(4,6.0)(4.9,7.35)( 7.1,7.35)( 8,6.0)
    \polygon(5,7.5)(6,9)(7,7.5)

    {%
      \color{white}
      \put(6,0.72){\makebox(0,0){Personne à personne}}
      \put(6,2.12){\makebox(0,0){Rapports \emph{ad hoc}}}
      \put(6,3.62){\makebox(0,0){Rapports programmés}}
      \put(6,5.12){\makebox(0,0){Apps}}
      \put(6,6.62){\makebox(0,0){Paquetages}}
      \put(6,8.02){\makebox(0,0){API}}
    }
    \put(12,-0,5){\makebox(0,0)[r]{%
        \smaller[2]%
        Adapté de
        \href{https://insurancedatascience.org/downloads/London2023/Mark_Sellors.pdf}{%
          Mark Sellors}}}
  \end{picture}
\end{frame}


\begin{frame}
  \frametitle{Objectifs}

  \begin{itemize}
  \item Vérifier empiriquement les propriétés des estimateurs des
    paramètres de structure dans le modèle de Bühlmann--Straub
  \item Se familiariser avec l'approche expérimentale en recherche
  \item Développer ses compétences en programmation R
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Résultats théoriques}

  %\larger
    \begin{align*}
      \esp{X_{ww}} &= m &
      \visible<2->{\esp{X_{zw}} &= m} &
      \visible<3->{\var{X_{zw}} &< \var{X_{ww}}} \\[6pt]
      \esp{\hat{s}^2} &= s^2 \\[6pt]
      \esp{\hat{a}} &= a &
      \visible<2->{\esp{\tilde{a}} &= a} &
      \visible<3->{\var{\tilde{a}} &< \var{\hat{a}}}
    \end{align*}
    \bigskip
    \visible<4->{
      \hspace*{0.36\linewidth}
      \begin{minipage}{0.53\linewidth}
        \centering
        \textcolor{alert}{\rule{\linewidth}{1pt}} \\
        pseudo-estimateurs!
      \end{minipage}}
\end{frame}

\begin{frame}
  \frametitle{Procédure expérimentale (ou algorithme)}

  \begin{enumerate}
  \item Simuler des poids $w_{it}$ pour $i = 1, \dots, I$ et $t = 1,
    \dots, n$
  \item Simuler des ratios $X_{it}$
  \item Calculer $X_{ww}$, $X_{zw}$, $\hat{s}^2$, $\hat{a}$,
    $\tilde{a}$
  \item Répéter les étapes 2 et 3 $N$ fois
  \item Calculer la moyenne empirique et la variance empirique de tous
    les estimateurs
  \item Vérifier les résultats théoriques
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Modèle de simulation proposé}

  \begin{align*}
    \bar{w}_i &\sim U(a, b) \\
    w_{it} &\sim U\left( \frac{\bar{w}_i}{2}, \frac{3\bar{w}_i}{2} \right) \\
    \intertext{}
    S_{it}|\Theta_i = \theta_i
    &\sim \text{Poisson composée}(w_{it} \theta_i, \text{Log-normale}(\mu, \sigma)) \\
    \Theta_i
    &\sim \text{Gamma}(\alpha, \lambda) \\
    \intertext{}
    X_{it}
    &= \frac{S_{it}}{w_{it}}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Valeurs théoriques des paramètres de structure}

  Nous avons
  \begin{align*}
    \mu(\theta_i)
    &= \esp{X_{it}|\Theta_i = \theta_i} \\
    &= \theta_i e^{\mu + \sigma^2/2} \\
    \sigma^2(\theta_i)
    &= w_{it} \var{X_{it}|\Theta_i = \theta_i} \\
    &= \theta_i e^{2\mu + 2\sigma^2} \\
    \intertext{d'où}
      m
      &= \frac{\alpha}{\lambda}\, e^{\mu + \sigma^2/2} \\
      s^2
      &= \frac{\alpha}{\lambda}\, e^{2\mu + 2\sigma^2} \\
      a
      &= \frac{\alpha}{\lambda^2}\, e^{2\mu + \sigma^2} \\
      K
      &= \lambda e^{\sigma^2}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{À retenir}

  Quelques éléments à retenir lors de la préparation de votre code
  informatique.
  \begin{itemize}
  \item Tirez profit des fonctionnalités du langage R, comme le
    recyclage des vecteurs, pour éviter les manipulations inutiles
  \item Grâce à ces fonctionnalités, justement, la simulation d'un
    mélange continu ne requiert aucun outil spécial
  \item Là où un outil spécial est utile, c'est pour simuler une
    distribution composée: \pkg{actuar} propose \code{rcompois} et
    \code{rcompound}
  \item Pour vous épargner des manipulations de données, vous pouvez
    remplacer \code{cm} par une de ses fonctions auxiliaires
    \code{bstraub}; cette fonction n'étant pas exportée par
    \pkg{actuar}, il faut l'appeler avec \code{actuar:::bstraub}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{À retenir (suite)}

  \begin{itemize}
  \item Créez une fonction qui prend en arguments tous les paramètres
    du modèle de simulation et qui se charge de faire les calculs ---
    et de retourner les résultats pertinents --- pour une seule
    simulation
  \item Utilisez ensuite la fonction dans \code{replicate} pour
    obtenir les résultats de plusieurs simulations (sans boucle
    explicite!)
  \item Utilisez \code{set.seed} pour pouvoir répéter votre étude de
    simulation à l'identique
  \item Utilisez \code{saveRDS} ou \code{save.image} pour
    sauvegarder, respectivement, les résultats de la simulation ou
    tout l'espace de travail (et donc les résultats et le contexte
    dans lequel ils ont été obtenus)
  \end{itemize}
\end{frame}

%%% Local Variables:
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
