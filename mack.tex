%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Méthode de Mack}

\begin{frame}
  \frametitle{Présentation}

  \begin{itemize}
  \item Les méthodes Chain-Ladder et de Bornhuetter-Ferguson sont très
    efficaces pour estimer les réserves espérées, mais elles ne
    permettent pas d'en quantifier la variance et, donc, le risque
    associé aux estimations.
  \item \citet{Mack:chainladder:1993} fut un des premiers à fournir un
    cadre stochastique (non paramétrique) à la méthode Chain-Ladder.
  \item Ce cadre stochastique permet d'estimer la variance des
    projections.
  \item Nous avons déjà fourni une partie du cadre stochastique dans
    le chapitre sur la méthode Chain-Ladder.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Hypothèses}

  \begin{enumerate}
  \item Les montants cumulatifs des sinistres encourus $C_{i, j}$
    d'années d'accidents différentes sont indépendants.
  \item Il existe des facteurs de déroulement
    $\lambda_1, \dots, \lambda_{J - 1} > 0$ et des paramètres
    $\sigma_1^2, \dots, \sigma_{J - 1}^2 > 0$ tel que
    \begin{align*}
      \esp{C_{i, j}|C_{i, 1}, \dots, C_{i, j - 1}}
      &= \esp{C_{i, j}|C_{i, j - 1}} = \lambda_{j - 1} C_{i, j - 1} \\
      \var{C_{i, j}|C_{i, 1}, \dots, C_{i, j - 1}}
      &= \var{C_{i, j}|C_{i, j - 1}} = \sigma_{j - 1}^2 C_{i, j - 1}
    \end{align*}
    pour tout $i = 1, \dots, I$ et $j = 2, \dots, J$.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Notation}

  \begin{minipage}{0.45\textwidth}
    Nous avons défini en introduction l'ensemble des données
    disponibles dans le triangle de développement:
    \begin{equation*}
      \mathcal{D}_I \equiv \mathcal{D}_J =
      \{C_{i, j}; i + j - 1 \leq I, j \leq J\}.
    \end{equation*}
  \end{minipage}
  \hfill
  \begin{minipage}{0.45\textwidth}
    \footnotesize\centering
    Données $\mathcal{D}_5$ \\
    \begin{tabular}{crrrrr}
      \toprule
      & \multicolumn{5}{c}{$j$} \\
      $i$ & 1 & 2 & 3 & 4 & 5 \\
      \midrule
      1 & \alert{100} & \alert{150} & \alert{175} & \alert{180} & \alert{200} \\
      2 & \alert{110} & \alert{168} & \alert{192} & \alert{205} \\
      3 & \alert{115} & \alert{169} & \alert{202} \\
      4 & \alert{125} & \alert{185} \\
      5 & \alert{150} \\
      \bottomrule
    \end{tabular}
  \end{minipage}
  \medskip

  \begin{minipage}{0.45\textwidth}
    Définissons maintenant l'ensemble des données dans le triangle
    tronqué à l'âge $k$:
    \begin{equation*}
      \mathcal{D}_k =
      \{C_{i, j}; i + j - 1 \leq I, j \leq k\}.
    \end{equation*}
  \end{minipage}
  \hfill
  \begin{minipage}{0.45\textwidth}
    \footnotesize\centering
    Données $\mathcal{D}_3$ \\
    \begin{tabular}{crrrrr}
      \toprule
      & \multicolumn{5}{c}{$j$} \\
      $i$ & 1 & 2 & 3 & 4 & 5 \\
      \midrule
      1 & \alert{100} & \alert{150} & \alert{175} & 180 & 200 \\
      2 & \alert{110} & \alert{168} & \alert{192} & 205 \\
      3 & \alert{115} & \alert{169} & \alert{202} \\
      4 & \alert{125} & \alert{185} \\
      5 & \alert{150} \\
      \bottomrule
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Estimateurs des facteurs de déroulement}

  \begin{itemize}
  \item Nous utilisons l'estimateur Chain-Ladder pour estimer le
    facteur de déroulement $\lambda_j$, $j = 1, \dots, J - 1$:
    \begin{equation*}
      \hat{\lambda}_j = \frac{\sum_{i = 1}^{I - j} C_{i, j + 1}}{%
        \sum_{i = 1}^{I - j} C_{i, j}}.
    \end{equation*}
  \item On peut démontrer \citep[lemme~3.3]{Wuthrich:reserving:2008}
    que $\hat{\lambda}_j$ est l'estimateur linéaire \alert{sans biais} de
    $f_j$ à \alert{variance minimale} conditionnellement aux données
    $\mathcal{D}_j$.
  \item La variance conditionnelle de l'estimateur est
    \begin{equation*}
      \var{\hat{\lambda}_j|\mathcal{D}_j} =
      \frac{\sigma_j^2}{\sum_{i = 1}^{I - j} C_{i, j}}.
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Quelques propriétés de l'estimateur (1/5)}

  $\hat{\lambda}_j$ est un estimateur \alert{conditionnellement} sans biais
  de $\lambda_j$.

  \pause
  \begin{proof}
    \begin{align*}
      \esp{\hat{\lambda}_j|\mathcal{D}_j}
      &= \Esp{\left.\frac{\sum_{i = 1}^{I - j} C_{i, j + 1}}{%
        \sum_{i = 1}^{I - j} C_{i, j}}\right|\mathcal{D}_j} \\
      &= \frac{1}{\sum_{i = 1}^{I - j} C_{i, j}}
        \sum_{i = 1}^{I - j} \esp{C_{i, j + 1}|\mathcal{D}_j} \\
      &= \frac{1}{\sum_{i = 1}^{I - j} C_{i, j}}
        \sum_{i = 1}^{I - j} \lambda_j C_{i, j} \\
      &= \lambda_j
    \end{align*}
  \end{proof}
\end{frame}

\begin{frame}
  \frametitle{Quelques propriétés de l'estimateur (2/5)}

  $\hat{\lambda}_j$ est un estimateur sans biais de $\lambda_j$.

  \pause
  \begin{proof}
    Découle directement de la propriété précédente:
    \begin{align*}
      \esp{\hat{\lambda}_j}
      &= \esp{\esp{\hat{\lambda}_j|\mathcal{D}_j}} \\
      &= \esp{\lambda_j} \\
      &= \lambda_j
    \end{align*}
  \end{proof}
\end{frame}


\begin{frame}
  \frametitle{Quelques propriétés de l'estimateur (3/5)}

  Les estimateurs d'âges différents sont \alert{non correlés}.

  \pause
  \begin{proof}
    Pour $k < j$,
    \begin{align*}
      \esp{\hat{\lambda}_k \hat{\lambda}_j}
      &= \esp{\esp{\hat{\lambda}_k \hat{\lambda}_j|\mathcal{D}_j}} \\
      &= \esp{\hat{\lambda}_k \esp{\hat{\lambda}_j|\mathcal{D}_j}} \\
      &= \esp{\hat{\lambda}_k} \lambda_j \\
      &= \esp{\hat{\lambda}_k} \esp{\hat{\lambda}_j}.
    \end{align*}
    Par récurrence, on obtient que
    \begin{equation*}
      \esp{\hat{\lambda}_1 \hat{\lambda}_j \cdots \hat{\lambda}_{J - 1}} =
      \lambda_1 \lambda_j \cdots \lambda_{J - 1}.
    \end{equation*}
  \end{proof}
\end{frame}

\begin{frame}
  \frametitle{Quelques propriétés de l'estimateur (4/5)}

  Sachant $C_{i, I - i + 1}$, l'estimateur Chain-Ladder
  $\hat{C}_{i, J}^{\text{CL}}$ est un estimateur sans biais des
  sinistres cumulatifs espérés à l'ultime
  $\esp{C_{i, J}|\mathcal{D}_I} = \esp{C_{i, J}|C_{i, I - i + 1}}$.

  \pause
  \begin{proof}
    En premier lieu:
    \begin{align*}
      \esp{\hat{C}_{i, J}^{\text{CL}}|C_{i, I - i + 1}}
      &= \esp{C_{i, I - i + 1} \hat{\lambda}_{J - i} \cdots \hat{\lambda}_{J
        - 2} \hat{\lambda}_{J - 1}|C_{i, I - i + 1}} \\
      &= \esp{C_{i, I - i + 1} \hat{\lambda}_{J - i} \cdots \hat{\lambda}_{J
        - 2} \esp{\hat{\lambda}_{J - 1}|\mathcal{D}_{J - 1}}|C_{i, I - i + 1}} \\
      &= \lambda_{J - 1} \esp{\hat{C}_{i, J - 1}^{\text{CL}}|C_{i, I - i + 1}}
    \end{align*}
    Par récurrence, on obtient que
    \begin{equation*}
      \esp{\hat{C}_{i, J}^{\text{CL}}|C_{i, J - i + 1}} =
      C_{i, I - i + 1} \lambda_{I - i + 1} \cdots \lambda_{J - 1} =
      \esp{C_{i, J}|C_{i, J - i + 1}}.
    \end{equation*}
  \end{proof}
\end{frame}

\begin{frame}
  \frametitle{Quelques propriétés de l'estimateur (5/5)}

  L'estimateur Chain-Ladder $\hat{C}_{i, J}^{\text{CL}}$ est (non
  conditionnellement) un estimateur sans biais des sinistres
  cumulatifs espérés à l'ultime $\esp{C_{i, J}}$.

  \pause
  \begin{proof}
    Conséquence directe du résultat précédent.
  \end{proof}
\end{frame}

%% Note to self: il faut mieux préciser la définition de la provision
%% (et non de son estimateur). Important lorsque l'on utilise le fait
%% que MSE(hat(R)_i) == MSE(hat(C)_{i, J}), plus loin.

\begin{frame}
  \frametitle{Conséquence sur l'estimateur de la provision}

  L'estimateur Chain-Ladder $\hat{R}_i^{\text{CL}}$ de la provision de
  l'année d'accident $i$ est un estimateur sans biais de la provision
  $R_i$:
  \begin{align*}
    \esp{\hat{R}_i^{\text{CL}}}
    &= \esp{\hat{C}_{i, J}^{\text{CL}}} - C_{i, I - i + 1} \\
    &= \esp{C_{i, J}} - C_{i, I - i + 1} \\
    &= R_i.
  \end{align*}
\end{frame}

\begin{frame}[standout]
  Nous avons démontré que nos estimateurs \\
  Chain-Ladder sont sans biais.

  Nous ne connaissons toujours pas leur variance, élément essentiel
  pour établir le risque de ces estimations.
\end{frame}

\begin{frame}
  \frametitle{Estimateurs des paramètres de variance}

  \begin{itemize}
  \item Rappel: nous avons posé
    \begin{align*}
      \var{C_{i, j + 1}|C_{i, j}}
      &= \sigma_j^2 C_{i, j} \\
      \intertext{ou, exprimé différemment,}
      \sigma_j^2
      &= C_{i, j} \Var{\left.\frac{C_{i, j + 1}}{C_{i, j}}\right|C_{i, j}}.
    \end{align*}
  \item Notre estimateur du paramètre de variance $\sigma_j^2$ est
    \begin{equation*}
      \hat{\sigma}_j^2 = \frac{1}{I - j - 1}
      \sum_{i = 1}^{I - j} C_{i, j}
      \left(
        \frac{C_{i, j + 1}}{C_{i, j}} - \hat{\lambda}_j
      \right)^2.
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propriété de l'estimateur de variance}

  $\hat{\sigma}_j^2$ est un estimateur sans biais de $\sigma_j^2$.

  \pause
  \begin{block}{Démonstration (début)}
    Nous allons démontrer l'absence de biais conditionnellement à
    $\mathcal{D}_j$:
    \begin{equation*}
      \esp{\hat{\sigma}_j^2|\mathcal{D}_j} = \sigma_j^2.
    \end{equation*}
    Le résultat
    \begin{equation*}
      \esp{\hat{\sigma}_j^2} = \sigma_j^2.
    \end{equation*}
    est ensuite une conséquence directe.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Propriété de l'estimateur de variance}

  \begin{block}{Démonstration (suite)}
    En premier lieu,
    \begin{align*}
      \Esp{
      \left.\left(
      \frac{C_{i, j + 1}}{C_{i, j}} - \hat{\lambda}_j
      \right)^2 \right| \mathcal{D}_j}
      &= \Esp{
        \left.\left(
        \frac{C_{i, j + 1}}{C_{i, j}} - \lambda_j
        \right)^2 \right| \mathcal{D}_j} \\
      &\phantom{=} -
        2 \Esp{
        \left.\left(
        \frac{C_{i, j + 1}}{C_{i, j}} - \lambda_j
        \right) (\hat{\lambda}_j - \lambda_j) \right| \mathcal{D}_j} \\
      &\phantom{=} +
        \esp{
        (\hat{\lambda}_j- \lambda_j)^2 | \mathcal{D}_j}.
    \end{align*}
    Calculons maintenant chacun des termes du côté droit.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Propriété de l'estimateur de variance}

  \begin{block}{Démonstration (suite)}
    \begin{enumerate}
    \item On a
      \begin{align*}
        \Esp{
        \left.\left(
        \frac{C_{i, j + 1}}{C_{i, j}} - \lambda_j\right)^2 \right|
        \mathcal{D}_j}
        &= \Var{
          \left. \frac{C_{i,j+1}}{C_{i,j}} \right| \mathcal{D}_j} \\
        &= \frac{\sigma_j^2}{C_{i, j}}.
      \end{align*}
    \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Propriété de l'estimateur de variance}

  \begin{block}{Démonstration (suite)}
    \begin{enumerate}
      \setcounter{enumi}{1}
    \item Par indépendance entre les années d'accident,
      \begin{align*}
        \Esp{
        \left.\left(
        \frac{C_{i, j + 1}}{C_{i, j}} - \lambda_j
        \right) (\hat{\lambda}_j - \lambda_j) \right| \mathcal{D}_j}
        &= \Cov \left(\left.
          \frac{C_{i, j + 1}}{C_{i, j}}, \hat{\lambda}_j \right| \mathcal{D}_j
          \right) \\
        &= \frac{C_{i, j}}{\sum_{i = 1}^{I - j} C_{i, j}}
          \Var{\left.
          \frac{C_{i, j + 1}}{C_{i, j}} \right| \mathcal{D}_j} \\
        &= \frac{\sigma_j^2}{\sum_{i = 1}^{I - j} C_{i, j}}.
      \end{align*}
    \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Propriété de l'estimateur de variance}

  \begin{block}{Démonstration (suite et fin).}
    \begin{enumerate}
      \setcounter{enumi}{2}
    \item Par définition, le dernier terme est
      \begin{align*}
        \esp{
        (\hat{\lambda}_j- \lambda_j)^2 | \mathcal{D}_j}
        &= \var{\hat{\lambda}_j | \mathcal{D}_j} \\
        &= \frac{\sigma_j^2}{\sum_{i = 1}^{I - j} C_{i, j}}.
      \end{align*}
    \end{enumerate}
    Il est laissé en \alert{exercice} d'assembler tous les morceaux
    pour compléter la démonstration. %
    \qed
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Remarque}

  \begin{itemize}
  \item Lorsque $j = J - 1$ et $I = J$, l'estimateur du dernier
    paramètre de variance est
    \begin{equation*}
      \hat{\sigma}_{J - 1}^2 = \frac{1}{I - (J - 1) - 1} C_{1, J - 1}
      \left(
        \frac{C_{1, J}}{C_{1, J - 1}} - \hat{\lambda}_{J - 1}
      \right)^2,
    \end{equation*}
    ce qui donne une division par $0$.
  \item En pratique, on utilise alors
    \begin{equation*}
      \hat{\sigma}_{J - 1}^2 = \min
      \left(
        \frac{\hat{\sigma}_{J - 2}^4}{\hat{\sigma}_{J - 3}^2},
        \hat{\sigma}_{J - 3}^2,
        \hat{\sigma}_{J - 2}^2
      \right).
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Erreur de prévision}

  L'erreur quadratique moyenne de prévision de la provision de l'année
  d'accident $i$ est
  \begin{align*}
    \text{MSEP}(\hat{R}_i)
    &= \esp{(\hat{R}_i - R_i)^2|\mathcal{D}_I} \\
    &= \var{R_i|\mathcal{D}_I} + (\hat{R}_i - \esp{R_i|\mathcal{D}_I})^2.
  \end{align*}

  \begin{center}
    \citep[\dots\ \emph{plusieurs} étapes; voir]%
      [section 3.2\dots]{Wuthrich:reserving:2008}
  \end{center}

  Nous utilisons comme estimateur de cette erreur:
  \begin{equation*}
    \widehat{\text{MSEP}}(\hat{R}_i) = (\hat{C}_{i, J}^{\text{CL}})^2
    \sum_{k = I - i + 1}^{J - 1} \frac{\hat{\sigma}_k^2}{\hat{\lambda}_k^2}
    \left(
      \frac{1}{\hat{C}_{i, k}^{\text{CL}}}
      + \frac{1}{\sum_{i = 1}^{I - k} C_{i, k}}
    \right)
  \end{equation*}
  pour $i = 2, \dots, I$ et où
  $\hat{C}_{i, k}^{\text{CL}} = C_{i, I - i + 1} \hat{\lambda}_{I - i + 1}
  \cdots \hat{\lambda}_{k - 1}$, $k > I - i + 1$, sont les projections des
  valeurs futures des $C_{i, k}$, et
  $\hat{C}_{i, I - i + 1}^{\text{CL}} = C_{i, I - i + 1}$.
\end{frame}

\begin{frame}
  \frametitle{Examen de la formule de l'erreur de prévision (1/3)}

  Afin de mieux visualiser la formule, développons-la pour $i =
  1, 2, 3$.

  Le diagramme sur la droite représente un triangle de développement
  complété. Les points $\bullet$ sont des données connues et les
  astérisques $\ast$ sont des données projetées par la méthode
  Chain-Ladder. %
  \bigskip

  \begin{minipage}{0.65\linewidth}
    \small
    \begin{equation*}
      \widehat{\text{MSEP}}(\hat{R}_2) = (\hat{C}_{2, J}^{\text{CL}})^2
      \frac{\hat{\sigma}_{J - 1}^2}{\hat{\lambda}_{J - 1}^2}
      \left(
        \frac{1}{\color{alert}{C_{2, J - 1}}}
        + \frac{1}{\color{alert}{C_{1, J - 1}}}
      \right)
    \end{equation*}
  \end{minipage}
  \hfill
  \begin{minipage}{0.25\linewidth}
    \begin{tabular}{ccccc}
      $\bullet$ & $\bullet$ & $\bullet$ & \color{alert}{$\bullet$} & $\bullet$ \\
      $\bullet$ & $\bullet$ & $\bullet$ & \color{alert}{$\bullet$} & $\ast$ \\
      $\bullet$ & $\bullet$ & $\bullet$ & $\ast$ & $\ast$ \\
      $\bullet$ & $\bullet$ & $\ast$ & $\ast$ & $\ast$\\
      $\bullet$ & $\ast$ & $\ast$ & $\ast$ & $\ast$ \\
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Examen de la formule de l'erreur de prévision (2/3)}

  \begin{minipage}{0.65\linewidth}
    \small
    \begin{align*}
      \widehat{\text{MSEP}}(\hat{R}_3) &= (\hat{C}_{3, J}^{\text{CL}})^2
      \left[
        \frac{\hat{\sigma}_{J - 2}^2}{\hat{\lambda}_{J - 2}^2}
        \left(
          \frac{1}{\color{alert}{C_{3, J - 2}}}
          + \frac{1}{\color{alert}{C_{1, J - 2}} + \color{alert}{C_{2, J - 2}}}
                                         \right)
                                         \right. \\
      &\phantom{=} + \left.
        \frac{\hat{\sigma}_{J - 1}^2}{\hat{\lambda}_{J - 1}^2}
        \left(
          \frac{1}{\color{link}{\hat{C}_{3, J - 1}^{\text{CL}}}}
          + \frac{1}{\color{link}{C_{1, J - 1}}}
        \right)
      \right]
    \end{align*}
  \end{minipage}
  \hfill
  \begin{minipage}{0.25\linewidth}
    \begin{tabular}{ccccc}
      $\bullet$ & $\bullet$ & \color{alert}{$\bullet$} & \color{link}{$\bullet$} & $\bullet$ \\
      $\bullet$ & $\bullet$ & \color{alert}{$\bullet$} & $\bullet$ & $\ast$ \\
      $\bullet$ & $\bullet$ & \color{alert}{$\bullet$} & \color{link}{$\ast$} & $\ast$ \\
      $\bullet$ & $\bullet$ & $\ast$ & $\ast$ & $\ast$\\
      $\bullet$ & $\ast$ & $\ast$ & $\ast$ & $\ast$ \\
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Examen de la formule de l'erreur de prévision (3/3)}

  \begin{minipage}{0.65\linewidth}
    \small
    \begin{align*}
      \widehat{\text{MSEP}}(\hat{R}_4)
      &= (\hat{C}_{4, J}^{\text{CL}})^2
        \left[
        \frac{\hat{\sigma}_{J - 3}^2}{\hat{\lambda}_{J - 3}^2}
        \left(
          \frac{1}{\color{alert}{C_{4, J - 3}}} +
          \frac{1}{\color{alert}{C_{1, J - 3}}
                   + \color{alert}{C_{2, J - 3}}
                   + \color{alert}{C_{3, J - 3}}}
        \right)
        \right. \\
      &\phantom{=} +
        \frac{\hat{\sigma}_{J - 2}^2}{\hat{\lambda}_{J - 2}^2}
        \left(
          \frac{1}{\color{link}{\hat{C}_{4, J - 2}^{\text{CL}}}} +
          \frac{1}{\color{link}{C_{1, J - 2}}
                   + \color{link}{C_{2, J - 2}}}
        \right) \\
      &\phantom{=} +
        \frac{\hat{\sigma}_{J - 1}^2}{\hat{\lambda}_{J - 1}^2}
        \left.
        \left(
          \frac{1}{\color{url}{\hat{C}_{4, J - 1}^{\text{CL}}}}
          + \frac{1}{\color{url}{C_{1, J - 1}}}
        \right)
        \right]
    \end{align*}
  \end{minipage}
  \hfill
  \begin{minipage}{0.25\linewidth}
    \begin{tabular}{ccccc}
      $\bullet$ & \color{alert}{$\bullet$} & \color{link}{$\bullet$} & \color{url}{$\bullet$} & $\bullet$ \\
      $\bullet$ & \color{alert}{$\bullet$} & \color{link}{$\bullet$} & $\bullet$ & $\ast$ \\
      $\bullet$ & \color{alert}{$\bullet$} & $\bullet$ & $\ast$ & $\ast$ \\
      $\bullet$ & \color{alert}{$\bullet$} & \color{link}{$\ast$} & \color{url}{$\ast$} & $\ast$\\
      $\bullet$ & $\ast$ & $\ast$ & $\ast$ & $\ast$ \\
    \end{tabular}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Intervalle de confiance pour la provision}

  Deux options.

  \begin{enumerate}
  \item On suppose que
    \begin{equation*}
      R_i \sim N(\mu_i, \sigma_i^2)
    \end{equation*}
    de sorte qu'un intervalle de confiance pour $R_i$ est
    \begin{equation*}
      \left(
        \hat{R}_i - \zeta_{\alpha/2} \sqrt{\widehat{\text{MSEP}}(\hat{R}_i)},
        \hat{R}_i + \zeta_{\alpha/2} \sqrt{\widehat{\text{MSEP}}(\hat{R}_i)}
      \right).
    \end{equation*}

    Problème: la borne inférieure peut être négative.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Intervalle de confiance pour la provision}

  \begin{enumerate}
    \setcounter{enumi}{1}
  \item On suppose que
    \begin{equation*}
      R_i \sim \text{Log-normale}(\mu_i, \sigma_i^2).
    \end{equation*}
    de sorte qu'un intervalle de confiance pour $R_i$ est
    \begin{equation*}
      \left(
        e^{\mu_i - \zeta_{\alpha/2} \sigma_i},
        e^{\mu_i + \zeta_{\alpha/2} \sigma_i}
      \right),
    \end{equation*}
    avec
    \begin{align*}
      \mu_i &= \ln (\hat{R}_i) - \frac{\sigma_i^2}{2} \\
      \sigma_i^2 &= \ln \left( 1 + \frac{\widehat{\text{MSEP}}(\hat{R}_i)}{\hat{R}_i^2} \right).
    \end{align*}

    (Voir diapositive suivante.)
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Apparté}

  Si $X \sim \text{Log-normale}(\mu, \sigma^2)$, alors
  \begin{align*}
    \esp{X} &= e^{\mu + \sigma^2/2} \\
    \var{X} &= e^{2\mu + \sigma^2} (e^{\sigma^2} - 1) = \esp{X}^2 (e^{\sigma^2} - 1),
  \end{align*}
  d'où
  \begin{align*}
    \mu &= \ln (\esp{X}) - \frac{\sigma^2}{2} \\
    \sigma^2 &= \ln \left( 1 + \frac{\var{X}}{\esp{X}^2} \right).
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Dernière étape: erreur de prévision de la provision
    totale}

  L'erreur quadratique moyenne de prévision de la provision totale est
  \begin{equation*}
    \text{MSEP}(\hat{R}) =
    \Esp{\left. \sum_{i = 2}^I (\hat{R}_i - R_i)^2 \right| \mathcal{D}_I}.
  \end{equation*}

  L'estimateur de cette erreur est:
  \begin{align*}
    \widehat{\text{MSEP}}(\hat{R})
    &= \sum_{i = 2}^I \widehat{\text{MSEP}}(\hat{R}_i)
      + 2 \sum_{2 \leq i < k \leq I}
      \hat{C}_{i, J}^{\text{CL}} \hat{C}_{k, J}^{\text{CL}}
      \sum_{j = I - i + 1}^{J - 1}
      \frac{\hat{\sigma}_j^2/\hat{\lambda}_j^2}{\sum_{n = 1}^{I - j} C_{n, j}} \\
    &= \sum_{i = 2}^I
      \left\{
      \widehat{\text{MSEP}}(\hat{R}_i)
      + \hat{C}_{i, J}^{\text{CL}}
      \left(
      \sum_{k = i + 1}^I \hat{C}_{k, J}^{\text{CL}}
      \right)
      \sum_{j = I - i + 1}^{J - 1}
      \frac{2 \hat{\sigma}_j^2/\hat{\lambda}_j^2}{\sum_{n = 1}^{I - j} C_{n,
      j}}
      \right\}.
  \end{align*}

  Il est laissé en \alert{exercice} de dériver les formules des
  intervalles de confiance pour la provision totale.
\end{frame}

\begin{frame}
  \frametitle{Exemple}

  Résultats pour le triangle de développement utilisé dans les
  chapitres précédents.

  \begin{center}
    \begin{tabular}{crrrrrrr}
      \toprule
      & \multicolumn{5}{c}{Développement (âge)} \\
      Année & 1 & 2 & 3 & 4 & 5 & Provision & Écart type \\
      \midrule
      1 & 100 & 150    & 175    & 180    & 200    &   0,00  &   --- \\
      2 & 110 & 168    & 192    & 205    & 227,78 &  22,78  &  7,13 \\
      3 & 115 & 169    & 202    & 211,91 & 235,45 &  33,45  & 10,38 \\
      4 & 125 & 185    & 216,15 & 226,75 & 251,95 &  66,95  & 12,62 \\
      5 & 150 & 224,00 & 261,72 & 274,55 & 305,06 & 155,06  & 15,38 \\
      \midrule
      $\hat{\lambda}_j$ & $1,493$ & $1,168$ & $1,049$ & $1,111$ \\
      $\hat{s}_j^2$ & $0,073$ & $0,116$ & $0,140$ & $0,116$ \\
      \midrule
      TOTAL & & & & & & 278,24 & 33,64\\
      \bottomrule
    \end{tabular}
  \end{center}

  \medskip
  \gotoR{mack.R}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
