%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage[round]{natbib}             % references
  \usepackage[noae]{Sweave}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % various icons
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{changepage}                % page licence
  \usepackage{pict2e}                    % diagramme Git
  \usepackage{tabularx}                  % page licence
  \usepackage{relsize}                   % \smaller et al.
  \usepackage{booktabs}                  % beaux tableaux
  \usepackage{listings}                  % code source
  \usepackage{framed}                    % env. leftbar
  \usepackage[overlay,absolute]{textpos} % covers
  \usepackage{metalogo}                  % logo \XeLaTeX

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{ACT-2008 Mathématiques actuarielles IARD II}
  \author{Vincent Goulet}
  \renewcommand{\year}{2025}
  \renewcommand{\month}{01}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Beamer theme
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  %% -----
  %% Symboles manquants dans Fira Math
  %% https://tex.stackexchange.com/q/540732
  \setmathfont{texgyredejavu-math.otf}[range={\vdots,\ddots}]
  \setmathfont{Fira Math}[range=]
  %% -----
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Additionnal colors
  \definecolor{comments}{rgb}{0.7,0,0}      % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % internal links
  \definecolor{url}{rgb}{0.6,0,0}           % external links
  \definecolor{codebg}{named}{LightYellow1} % fond code R
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % UL red stripe
  \definecolor{or}{rgb}{1,0.8,0}            % UL yellow stripe
  \colorlet{alert}{mLightBrown} % alias for Metropolis color
  \colorlet{dark}{mDarkTeal}    % alias for Metropolis color
  \colorlet{shadecolor}{codebg}
  \definecolor{darkred}{rgb}{0.75,0,0}      % démo Cov(X, Y)
  \definecolor{darkgreen}{rgb}{0,0.75,0}    % démo Cov(X, Y)
  \definecolor{darkblue}{rgb}{0,0,0.75}     % démo Cov(X, Y)
  %% palette Okabe-Ito
  \definecolor{OIGold}{RGB}{230,159,0}     % orange
  \definecolor{OISkyBlue}{RGB}{86,180,233} % bleu clair
  \definecolor{OIGreen}{RGB}{0,158,115}    % vert émeraude
  \definecolor{OIYellow}{RGB}{240,228,66}  % jaune
  \definecolor{OIBlue}{RGB}{0,114,178}     % bleu
  \definecolor{OIRed}{RGB}{213,94,0}       % rouge vermillion
  \definecolor{OIPink}{RGB}{204,121,167}   % rose pourpre

  %% Hyperlinks
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Mathématiques actuarielles IARD II - Théorie de la crédibilité},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Redéfinition des noms de partie changés par Babel pour éviter de
  %% se retrouver avec un titre «Première partie I» sur une page de
  %% partie.
  \addto\captionsfrench{\def\partname{Partie}}

  %% Sections de code source
  \lstloadlanguages{R}
  \lstset{language=R,
    extendedchars=true,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    escapeinside=`',
    aboveskip=0pt,
    belowskip=0pt,
    showstringspaces=false}

  %% Bibliography
  %% Couple of hacks needed to have beamer and natbib play nice with
  %% each other.
  \renewcommand{\newblock}{}    % https://tex.stackexchange.com/a/1971/24355
  \renewcommand{\bibsection}{}  % drop \section heading
  \bibliographystyle{francais}

  %% ===============================
  %%  New commands and environments
  %% ===============================

  %% Environnements de Sweave.
  %%
  %% Les environnements Sinput et Soutput utilisent Verbatim (de
  %% fancyvrb). On les réinitialise pour enlever la configuration par
  %% défaut de Sweave, puis on réduit l'écart entre les blocs Sinput
  %% et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{fontsize=\small,listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \renewenvironment{Schunk}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Theorem-like environments
  \theoremstyle{plain}
  \newtheorem{thm}{Théorème}
  \newtheorem{corollaire}{Corollaire}
  \theoremstyle{definition}
  \newtheorem{consequence}{Consequence}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\texttt{#1}}
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\link}[2]{\href{#1}{#2~\raisebox{-0.2ex}{\faExternalLink}}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Renvois vers les scripts R
  \newcommand{\gotoR}[1]{%
    \begin{center}
      \colorbox{mDarkTeal}{\color{white}
        \rule{3mm}{0pt}\raisebox{-1pt}{\large\faMapSigns}\;%
        {\ttfamily #1}\rule{3mm}{0pt}}
    \end{center}}

  %% Commandes usuelles vg
  \newcommand{\pt}{{\scriptscriptstyle \Sigma}}
  \newcommand{\Esp}[1]{E\! \left[ #1 \right]}
  \newcommand{\hEsp}[1]{\hat{E}\! \left[ #1 \right]}
  \newcommand{\esp}[1]{E [ #1 ]}
  \newcommand{\hesp}[1]{\hat{E} [ #1 ]}
  \newcommand{\Var}[1]{\operatorname{Var}\! \left[ #1 \right]}
  \newcommand{\var}[1]{\operatorname{Var} [ #1 ]}
  \newcommand{\Cov}{\operatorname{Cov}}
  \newcommand{\CV}{\operatorname{CV}}
  \newcommand{\mat}[1]{\symbf{#1}}
  \newcommand{\Z}{\mathbb{Z}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Lengths used to compose front and rear covers.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

  %% Longueurs et type de colonne pour les tableaux de l'introduction
  \newlength{\tablewidth}
  \setlength{\tablewidth}{\textwidth}
  \addtolength{\tablewidth}{-2\parindent}
  \newcolumntype{Y}{>{\centering\arraybackslash}X}

%  \includeonly{couverture-avant,laboratoire-secret}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}
  \frametitle{Sommaire}

  \begin{columns}
    \column{0.5\textwidth}
    Partie I --- Théorie de la crédibilité \\[6pt]
    \tableofcontents[part=1]

    \column{0.5\textwidth}
    Partie II --- Provisionnement \\[6pt]
    \tableofcontents[part=2]
  \end{columns}
\end{frame}

%% mainmatter

\include{preambule}

\part{Théorie de la crédibilité}

\begin{frame}
  \partpage
\end{frame}

\include{introduction-credibilite}
\include{stabilite}
\include{bayesienne}
\include{intermede-pre-buhlmann}
\include{buhlmann}
\include{intermede-pre-buhlmann-straub}
\include{buhlmann-straub}

\part{Provisionnement en assurance IARD}

\begin{frame}
  \partpage
\end{frame}

\include{introduction-provisionnement}
\include{chain-ladder}
\include{bornhuetter-ferguson}
\include{mack}
\include{regression}
\include{exercices}

%% annexe
\appendix
\include{travail-pratique}
%\include{laboratoire-secret}

%% backmatter
\include{bibliographie}
\include{colophon}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
