%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «ACT-2008 Mathématiques actuarielles IARD II»
%%% https://gitlab.com/vigou3/mathematiques-actuarielles-iard-ii.git
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Travail pratique}

\begin{frame}
  \frametitle{Objectifs}

  \begin{itemize}
  \item Résoudre un problème concret à l'aide de la théorie de la
    crédibilité
  \item Choisir un modèle de prévision parmi plusieurs options
  \item Évaluer ses méthodes de travail en contexte collaboratif
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mandat}

  \begin{enumerate}
  \item Élaborer un modèle de prévision du total d'une ronde de golf
    d'un quatuor à partir de ses pointages aux trous précédents
  \item Concevoir et mettre en production un prototype d'application
    de calcul des prévisions à l'aide de la technologie Shiny
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Mon travail pratique en 180 secondes}

  \begin{itemize}
  \item Objectif: expliquer votre proposition\dots\ en 180 secondes
  \item Soyez «3C»: clairs, concis, convaincants
  \item Et un quatrième: créatifs
  \item Pas nécessairement tous les membres de l'équipe
  \end{itemize}
  \pause

  \warningbox{Le concept de \emph{Dans l'oeil du dragon} est usé à la
    corde.}
\end{frame}

\begin{frame}
  \frametitle{Exigences relatives à l'utilisation de Git}

  \begin{itemize}
  \item Branche de développement
  \item Étiquettes
  \item Contribution de tous les membres de l'équipe
  \end{itemize}

  \begin{center}
    \setlength{\unitlength}{7mm}
    \linethickness{1.2pt}
    \small
    \begin{picture}(18.5,3.5)(0.75,0.5)
      \newsavebox{\cmst}      % commit branche master
      \newsavebox{\cdev}      % commit branche devel
      \newsavebox{\ccor}      % commit branches correction
      \newsavebox{\mergedn}   % embranchement vers le bas
      \newsavebox{\mergeup}   % embranchement vers le haut
      \savebox{\cmst}(0,0){%
        \textcolor{LightBlue2}{\circle*{0.5}}\circle{0.5}}
      \savebox{\cdev}(0,0){%
        \textcolor{Orchid2}{\circle*{0.5}}\circle{0.5}}
      \savebox{\ccor}(0,0){%
        \textcolor{PaleGreen2}{\circle*{0.5}}\circle{0.5}}
      \savebox{\mergedn}(0,0){%
        \Line(0,0)(0.25,0)
        \qbezier(0.25,0)(0.75,0)(0.75,-0.5)
        \qbezier(0.75,-0.5)(0.75,-1)(1.25,-1)
        \Line(1.25,-1)(1.5,-1)}
      \savebox{\mergeup}(0,0){%
        \Line(0,0)(0.25,0)
        \qbezier(0.25,0)(0.75,0)(0.75,0.5)
        \qbezier(0.75,0.5)(0.75,1)(1.25,1)
        \Line(1.25,1)(1.5,1)}

      %% lignes
      \Line(  1,2)(17.5,2)         % branche master
      \Line(2.5,1)(17.5,1)         % branche devel
      \put(1,2){\usebox{\mergedn}} % embranchement vers devel
      \Line( 4.5,1)( 4.5,2)        % étiquette 1
      \Line(   8,1)(   8,2)        % étiquette 2
      \Line(11.5,1)(11.5,2)        % étiquette 3
      \Line(  15,1)(  15,2)        % étiquette 4
      \Line(17.5,1)(17.5,2)        % étiquette 5
      \put( 4.5,2){\usebox{\mergeup}} % embranchement correction 1
      \put(   8,2){\usebox{\mergeup}} % embranchement correction 2
      \put(11.5,2){\usebox{\mergeup}} % embranchement correction 3
      \put(17.5,2){\usebox{\mergeup}} % embranchement correction 4

      %% étiquettes
      \put( 4.5,2.65){\makebox(0,0){\faTag}}
      \put(   8,2.65){\makebox(0,0){\faTag}}
      \put(11.5,2.65){\makebox(0,0){\faTag}}
      \put(  15,2.65){\makebox(0,0){\faTag}}
      \put(17.5,2.65){\makebox(0,0){\faTag}}

      %% branche master
      \put(   1,2){\usebox{\cmst}}
      \put( 4.5,2){\usebox{\cmst}}
      \put(   8,2){\usebox{\cmst}}
      \put(11.5,2){\usebox{\cmst}}
      \put(  15,2){\usebox{\cmst}}
      \put(17.5,2){\usebox{\cmst}}

      %% branche devel
      \put( 2.5,1){\usebox{\cdev}}
      \put( 3.5,1){\usebox{\cdev}}
      \put( 4.5,1){\usebox{\cdev}}
      \put(   6,1){\usebox{\cdev}}
      \put(   7,1){\usebox{\cdev}}
      \put(   8,1){\usebox{\cdev}}
      \put(   9,1){\usebox{\cdev}}
      \put(  10.5,1){\usebox{\cdev}}
      \put(  11.5,1){\usebox{\cdev}}
      \put(  13,1){\usebox{\cdev}}
      \put(  14,1){\usebox{\cdev}}
      \put(  15,1){\usebox{\cdev}}
      \put(16.5,1){\usebox{\cdev}}
      \put(17.5,1){\usebox{\cdev}}

      %% branches de correction
      \put( 6.25,3){\usebox{\ccor}}
      \put( 9.75,3){\usebox{\ccor}}
      \put(13.25,3){\usebox{\ccor}}
      \put(19.25,3){\usebox{\ccor}}
    \end{picture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Recherche reproductible}
  \begin{itemize}
  \item Documentation et commentaires
  \item Fichiers de script portables (section 3.8 de «Programmer avec
    R», édition 2024.01 ou plus récente)
  \end{itemize}
\end{frame}

%%% Local Variables:
%%% TeX-master: "mathematiques-actuarielles-iard-ii"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
